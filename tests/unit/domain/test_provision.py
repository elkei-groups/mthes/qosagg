import pytest

from qosagg.domain.graph import Task
from qosagg.domain.provision import AttributeValue, Provision


def test_attribute_eq() -> None:
    # given two equal attributes
    a1 = AttributeValue("attr", "1.7")
    a2 = AttributeValue("attr", "1.7")

    # then hash and eq should work as expected
    assert a1 == a2
    assert not a1 != a2
    assert hash(a1) == hash(a2)


def test_attribute_neq() -> None:
    # given different attributes
    a = AttributeValue("attr", "1.7")
    a_name = AttributeValue("attr2", "1.7")
    a_value = AttributeValue("attr", "-1.7")

    # then hashes and eqs should state that all are different
    assert a != a_name
    assert a != a_value
    assert not a == a_name
    assert not a == a_value
    assert hash(a) != hash(a_name)
    assert hash(a) != hash(a_value)
    assert a != "foo"


def test_attribute_str() -> None:
    a = AttributeValue("attr", "1.7")
    assert str(a) == "attr = 1.7"


def test_provision_eq() -> None:
    # given two equal provisions
    p1 = Provision("prov", {Task("A"), Task("B")}, {AttributeValue("attr1", "1.0"), AttributeValue("attr2", "-2")})
    p2 = Provision("prov", {Task("B"), Task("A")}, {AttributeValue("attr2", "-2"), AttributeValue("attr1", "1.0")})

    # then eq should work as expected
    assert p1 == p2
    assert not p1 != p2


def test_provision_neq() -> None:
    # given different provisions
    p = Provision("prov", {Task("A"), Task("B")}, {AttributeValue("attr1", "1"), AttributeValue("attr2", "-2.0")})
    p_name = Provision("other_prov", {Task("A"), Task("B")},
                       {AttributeValue("attr1", "1"), AttributeValue("attr2", "-2.0")})
    p_tasks = Provision("prov", {Task("A"), Task("B"), Task("C")},
                        {AttributeValue("attr1", "1"), AttributeValue("attr2", "-2.0")})
    p_attributes = Provision("prov", {Task("A"), Task("B")},
                             {AttributeValue("attr1", "1"), AttributeValue("attr2", "2")})

    # then eq should state that all are different
    assert p != p_name
    assert p != p_tasks
    assert p != p_attributes
    assert p != "foo"


def test_provision_str() -> None:
    p = Provision("prov", {Task("A"), Task("B")}, {AttributeValue("attr1", "1.0"), AttributeValue("attr2", "-2.0")})
    assert str(p) == "provision prov for A, B: attr1 = 1.0, attr2 = -2.0" or \
           str(p) == "provision prov for B, A: attr1 = 1.0, attr2 = -2.0" or \
           str(p) == "provision prov for A, B: attr2 = -2.0, attr1 = 1.0" or \
           str(p) == "provision prov for B, A: attr2 = -2.0, attr1 = 1.0"


def test_get_attribute_values_and_names() -> None:
    p = Provision("prov", {Task("A"), Task("B")}, {AttributeValue("attr1", "1.0"), AttributeValue("attr2", "-2")})
    assert p.get_attribute_names() == {"attr1", "attr2"}
    assert p.get_attribute_value("attr1") == "1.0"
    assert p.get_attribute_value("attr2") == "-2"
    with pytest.raises(KeyError):
        p.get_attribute_value("foo")
