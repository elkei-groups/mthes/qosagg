from typing import List, Iterable

from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.graph import GraphComposition
from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph


class OutputGeneratingGraphVisitor(UGraphVisitor[List[str]]):
    def __init__(self, naming: NamingStrategy):
        self.naming = naming

    def visit_task_instance(self, task_inst: TaskInstance) -> List[str]:
        inst_name = self.naming.name_task_instance(task_inst)
        selection_array = self.naming.name_selection_array()
        selection = f"{selection_array}[{inst_name}]"
        names_array = self.naming.name_provision_names_array()
        return [f"\"{task_inst.task_occ.task}=\"", f"{names_array}[fix({selection})]"]

    def visit_composite_ugraph(self, composite: CompositeUGraph) -> List[str]:
        if composite.composition == GraphComposition.CHOICE:
            # only output the selected path
            choice_variable = self.naming.name_composite(composite)
            return [
                f"if fix({choice_variable}) == {subgraph_index} then {subgraph_part} else \"\" endif"
                for (subgraph_index, subgraph) in enumerate(composite.subgraphs, 1)
                for subgraph_part in self.visit(subgraph)
            ]
        else:
            if composite.composition == GraphComposition.SEQUENTIAL:
                # no parentheses around sequential composition due to highest precedence
                prefix = []
                suffix = []
            else:
                prefix = ["\"(\""]
                suffix = ["\")\""]
            middle = OutputGeneratingGraphVisitor._combine_composite_outputs(
                (self.visit(subgraph) for subgraph in composite.subgraphs),
                composite.composition
            )
            return prefix + middle + suffix

    @staticmethod
    def _combine_composite_outputs(parts: Iterable[List[str]], composition: GraphComposition) -> List[str]:
        return [string
                for part in parts if part
                for string in part + [f"\" {str(composition)} \""]
                ][:-1]
