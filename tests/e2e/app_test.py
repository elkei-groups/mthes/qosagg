import importlib.resources
import pathlib
import textwrap
from typing import Dict

import pytest
from _pytest.capture import CaptureFixture

import tests.e2e.in_out_files
from qosagg import app
from qosagg.codegen.base_minizinc_generator import MiniZincGeneratorConfig
from tests.e2e.minizinc_executor import MinizincExecutor


def test_run_shows_help(capsys: CaptureFixture[str]):
    # given some arguments containing --help
    args = ["-h", "foo.txt", "--bar ignorethis", "--help", "somemoreargs", "-x"]

    # when running the application
    with pytest.raises(SystemExit) as e:
        app.run(args)

    # then the app should have exited successfully
    assert e.type == SystemExit
    assert e.value.code == 0
    capture = capsys.readouterr()
    assert "usage: " in capture.out
    assert "" == capture.err


def test_run_shows_parsing_errors(tmp_path, capsys: CaptureFixture[str]):
    # given arguments with an invalid file as input
    outfile = tmp_path / "failing_out.mzn"
    infile = tmp_path / "failing_in.qag"
    with open(infile, "w") as writer:
        writer.write("this should not be parsable :)\n")
    args = [str(infile), str(outfile)]

    # when executing the app
    with pytest.raises(SystemExit) as e:
        app.run(args)

    # then there should have been no errors and the output should be what we expect
    assert e.type == SystemExit
    assert e.value.code == 1
    capture = capsys.readouterr()
    assert "Parsing failed at 1:0: mismatched input 'this' expecting <EOF>" in capture.err.strip()
    assert "" == capture.out


@pytest.mark.parametrize(["flag", "warning"], [
    ("--no-categories", "Warning: Category generation is disabled, but at least one workflow declares categories.\n"),
    ("--no-taskinst-map", "Warning: Category generation implicitly requires task instance mappings.\n")
])
def test_run_shows_categories_warnings(tmp_path, capsys: CaptureFixture[str], flag: str, warning: str):
    # given arguments with a file as input that has categories and the no-categories flag
    outfile = tmp_path / "out.mzn"
    infile = tmp_path / "in.qag"
    with open(infile, "w") as writer:
        writer.write("workflow { graph: A; provision a for A: provider = 1; category provider; }\n")
    args = [str(infile), str(outfile), flag]

    # when executing the app
    app.run(args)

    # then there should be a warning on stderr
    capture = capsys.readouterr()
    assert warning in capture.err


def test_run_shows_missing_attribute_values_error(tmp_path, capsys: CaptureFixture[str]):
    # given arguments with an invalid file as input
    outfile = tmp_path / "failing_out.mzn"
    infile = tmp_path / "failing_in.qag"
    with open(infile, "w") as writer:
        writer.write(textwrap.dedent("""\
            workflow {
                graph: A;
                provision p1 for A: cost = 5;
                provision p2 for A: cots = 4;
                aggregation cost: sum;
            }
        """.strip()))
    args = [str(infile), str(outfile)]

    # when executing the app
    with pytest.raises(SystemExit) as e:
        app.run(args)

    # then there should have been no errors and the output should be what we expect
    assert e.type == SystemExit
    assert e.value.code != 0
    capture = capsys.readouterr()
    assert "Missing value of attribute \"cost\" for provision \"p2\"" in capture.err.strip()
    assert "" == capture.out


success_examples = [
    ("noagg.qag", "noagg.txt", MiniZincGeneratorConfig()),
    ("agg_linear.qag", "agg_linear.txt", MiniZincGeneratorConfig()),
    ("agg_parallel.qag", "agg_parallel.txt", MiniZincGeneratorConfig()),
    ("agg_choice.qag", "agg_choice.txt", MiniZincGeneratorConfig()),
    ("multiple.qag", "multiple.txt", MiniZincGeneratorConfig()),
    ("agg_loop.qag", "agg_loop.txt", MiniZincGeneratorConfig()),
    ("cp_mix_unsat.qag", "unsatisfiable.txt", MiniZincGeneratorConfig(checkpoints=True)),
    ("cp_mix_sat.qag", "cp_mix_sat.txt", MiniZincGeneratorConfig(checkpoints=True)),
    ("cp_choice.qag", "cp_choice.txt", MiniZincGeneratorConfig(checkpoints=True, reachability=True)),
    ("looped_choice.qag", "looped_choice.txt", MiniZincGeneratorConfig(checkpoints=True, reachability=True)),
    ("looped_choice_2.qag", "looped_choice_2.txt", MiniZincGeneratorConfig(checkpoints=True, reachability=True)),
    ("reachability.qag", "reachability.txt", MiniZincGeneratorConfig(checkpoints=True, reachability=True)),
    ("unordered_provisions.qag", "unordered_provisions.txt",
     MiniZincGeneratorConfig(checkpoints=True, reachability=True)),
    ("vartypes.qag", "vartypes.txt", MiniZincGeneratorConfig()),
    ("constloop.qag", "constloop.txt", MiniZincGeneratorConfig()),
    ("taskinst_mapping.qag", "taskinst_mapping.txt", MiniZincGeneratorConfig(taskinst_mapping=True)),
    ("null.qag", "null.txt", MiniZincGeneratorConfig()),
    ("optional.qag", "optional.txt", MiniZincGeneratorConfig()),
    ("categories.qag", "categories.txt", MiniZincGeneratorConfig(categories=True, taskinst_mapping=True)),
    ("categories_reachability.qag", "categories_reachability.txt",
     MiniZincGeneratorConfig(categories=True, taskinst_mapping=True, reachability=True)),
    ("var_attribute.qag", "var_attribute.txt", MiniZincGeneratorConfig()),
    ("multi_occurrence.qag", "multi_occurrence.txt", MiniZincGeneratorConfig()),
    ("skateboards_bug.qag", "skateboards_bug.txt", MiniZincGeneratorConfig(checkpoints=True, reachability=True))
]


@pytest.mark.parametrize(["infile_name", "expected_output_file_name", "config"], success_examples)
def test_run_success(tmp_path: pathlib.Path, mzn_exec: MinizincExecutor, infile_name: str,
                     expected_output_file_name: str, config: MiniZincGeneratorConfig, capsys):
    # given arguments with the parameterized input file and a temporary file for output
    with importlib.resources.path(tests.e2e.in_out_files, infile_name) as infile:
        # execute QosAgg
        outfile = tmp_path / (infile_name + ".mzn")
        flags: Dict[str, bool] = {
            "--no-checkpoints": not config.checkpoints,
            "--no-reachability": not config.reachability,
            "--no-output": not config.output,
            "--no-taskinst-mapping": not config.taskinst_mapping,
            "--no-categories": not config.categories,
        }
        args = [str(infile), str(outfile)] + [flag for flag in flags if flags[flag]]

        # when executing the app
        app.run(args)
        print(outfile.read_text())

        # execute minizinc
        output = mzn_exec.solve(outfile)

        # then there should have been no errors and the output should be what we expect
        expectation = importlib.resources.read_text(tests.e2e.in_out_files, expected_output_file_name)
        assert output.strip("-= \t\n").replace("\t", "    ") == expectation.strip()
        capture = capsys.readouterr()
        assert "" == capture.err


def test_run_fails_with_arbitrary_attributes_without_slow_parser(tmp_path, capsys: CaptureFixture[str]):
    # when using non-primitive attributes but only the fast parser
    outfile = tmp_path / "failing_out.mzn"
    with importlib.resources.path(tests.e2e.in_out_files, "var_attribute.qag") as infile, \
            pytest.raises(SystemExit) as e:
        app.run([str(infile), str(outfile), "--no-non-primitive-attributes"])

    # then the app should output an error
    assert e.type == SystemExit
    assert e.value.code == 1
    capture = capsys.readouterr()
    assert "Parsing failed" in capture.err.strip()
    assert "" == capture.out
