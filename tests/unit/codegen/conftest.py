from io import StringIO
from typing import Generator

import pytest


@pytest.fixture()
def output() -> Generator[StringIO, None, None]:
    string_io = StringIO()
    yield string_io
    string_io.close()
