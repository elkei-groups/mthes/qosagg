from typing import Set

from qosagg.domain.graph import Task


class AttributeValue:
    # TODO there should be no name but instead an attribute object

    def __init__(self, name: str, value: str):
        self.name = name
        self.value = value

    def __eq__(self, other):
        if not isinstance(other, AttributeValue):
            return False
        return self.name == other.name and self.value == other.value

    def __str__(self) -> str:
        return f"{self.name} = {self.value}"

    def __hash__(self):
        return hash((self.name, self.value))


class Provision:
    def __init__(self, name: str, tasks: Set[Task], attributes: Set[AttributeValue]):
        self.name = name
        self.tasks = tasks
        self.attributes = attributes

    def get_attribute_names(self) -> Set[str]:
        return {a.name for a in self.attributes}

    def get_attribute_value(self, attribute_name: str) -> str:
        for a in self.attributes:
            if a.name == attribute_name:
                return a.value
        raise KeyError(f"No value for attribute {attribute_name} given")

    def __eq__(self, other):
        if not isinstance(other, Provision):
            return False
        return self.name == other.name and self.tasks == other.tasks and self.attributes == other.attributes

    def __str__(self) -> str:
        tasks = ", ".join(self.tasks)
        attributes = ", ".join([str(a) for a in self.attributes])
        return f"provision {self.name} for {tasks}: {attributes}"
