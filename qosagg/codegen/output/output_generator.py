from typing import TextIO, Iterable, Tuple

from qosagg.codegen.base_minizinc_generator import BaseMiniZincGenerator, MiniZincGeneratorConfig
from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.codegen.output.output_generating_graph_visitor import OutputGeneratingGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import UGraph
from qosagg.domain.workflow import NamedWorkflow


class OutputGenerator(BaseMiniZincGenerator):
    def __init__(self, workflow: NamedWorkflow[UGraph], file: TextIO, config: MiniZincGeneratorConfig,
                 naming: NamingStrategy):
        super().__init__(file, config, naming)
        self.workflow = workflow

    def generate_output(self) -> None:
        self._generate_provision_names()
        self._generate_graph_output()
        self._generate_aggregation_output()
        if self.config.categories:
            self._generate_categories_output()

    def _generate_provision_names(self) -> None:
        provision_enum = self.naming.name_provision_enum()
        names_array = self.naming.name_provision_names_array()
        values = [
            f"\"{name}\""
            for name in self.workflow.get_provision_names()
        ]
        # TODO gets broken once the order in the provisions enum changes
        self.generate_array(provision_enum, names_array, values, array_type="string", variable=False)

    def _generate_graph_output(self) -> None:
        graph_output = OutputGeneratingGraphVisitor(self.naming).visit(self.workflow.graph)
        label = f"\"Selection graph for {self.workflow.name}:\\n\\t\""
        newline = "\"\\n\""
        joined_graph_output = ", ".join([label] + graph_output + [newline])
        self.file.write(f"output [{joined_graph_output}];\n")

    def _generate_aggregation_output(self) -> None:
        if not self.workflow.aggregations:
            return
        self._generate_table_output(f"Aggregations for {self.workflow.name}", (
            (agg.attribute_name, self.naming.name_aggregation(agg)) for agg in self.workflow.aggregations
        ))

    def _generate_categories_output(self) -> None:
        if not self.workflow.categories:
            return
        rows = [(cat.attribute_name, self.naming.name_category_changes_variable(cat.attribute_name))
                for cat in self.workflow.categories]
        rows.append(("Σ changes", self.naming.name_all_category_changes_variable()))
        self._generate_table_output(f"Category changes for {self.workflow.name}", rows)

    def _generate_table_output(self, caption: str, rows: Iterable[Tuple[str, str]]) -> None:
        body = "".join(f",\n\t\"\\t{label}: \\({value})\\n\"" for label, value in rows)
        self.file.write(f"output [\"{caption}:\\n\"{body}];\n")
