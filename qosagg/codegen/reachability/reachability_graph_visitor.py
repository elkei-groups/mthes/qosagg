from collections import defaultdict
from typing import List, Dict

from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.graph import GraphComposition
from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph


class ReachabilityGraphVisitor(UGraphVisitor[None]):
    def __init__(self, naming: NamingStrategy):
        self._naming = naming
        self.reachabilities: Dict[TaskInstance, str] = defaultdict(lambda: "false")
        self._choice_stack: List[str] = []

    def visit_task_instance(self, task_inst: TaskInstance) -> None:
        if len(self._choice_stack) == 0:
            reachability = "true"
        else:
            reachability = " ∧ ".join(self._choice_stack)
        self.reachabilities[task_inst] = reachability

    def visit_composite_ugraph(self, composite: CompositeUGraph) -> None:
        if composite.composition == GraphComposition.CHOICE:  # add choice to stack and visit options
            variable = self._naming.name_composite(composite)
            for index, subgraph in enumerate(composite.subgraphs, 1):
                self._choice_stack.append(f"{variable} == {index}")
                self.visit(subgraph)
                self._choice_stack.pop()
        else:  # all the subgraphs are reachable
            for subgraph in composite.subgraphs:
                self.visit(subgraph)
