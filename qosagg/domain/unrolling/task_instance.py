from __future__ import annotations

from enum import Enum, unique, auto
from typing import NamedTuple, Collection, Tuple

from qosagg.domain.graph import TaskOccurrence

Iteration = Tuple[int, ...]


class TaskInstance(NamedTuple):
    task_occ: TaskOccurrence
    iteration: Iteration = ()

    def __hash__(self) -> int:
        return hash(tuple([self.task_occ, *self.iteration]))

    @property
    def checkpoints(self) -> Collection[Checkpoint]:
        return Checkpoint(self, CheckpointType.PRE), Checkpoint(self, CheckpointType.POST)


@unique
class CheckpointType(Enum):
    PRE = auto()
    POST = auto()


class Checkpoint(NamedTuple):
    task_instance: TaskInstance
    cp_type: CheckpointType
