from typing import List
from unittest.mock import call

from pytest_mock import MockerFixture

from qosagg.domain.graph import CompositeGraph, GraphComposition, LoopedGraph
from qosagg.domain.task_instance_visitor import TaskInstanceVisitor
from qosagg.domain.unrolling.unrolled_graph import unroll
from tests.helpers import sti, ssg


def test_visit_mixed_graph(mocker: MockerFixture):
    # given a mixed workflow: A | (B^2 + C D)^2
    graph = CompositeGraph([
        ssg("A"),
        LoopedGraph(
            CompositeGraph([
                LoopedGraph(ssg("B"), 2, "l2"),
                CompositeGraph([
                    ssg("C"),
                    ssg("D")
                ], GraphComposition.SEQUENTIAL, "s1")
            ], GraphComposition.CHOICE, "c1"),
            2, "l1")
    ], GraphComposition.PARALLEL, "p1")

    # when visiting the graph with a mock callback
    mock = mocker.MagicMock()
    TaskInstanceVisitor(mock).visit(unroll(graph))

    # then the callback should have been called with what we expect
    calls: List = [
        call(sti("A")),
        call(sti("B", (0, 0))),
        call(sti("B", (0, 1))),
        call(sti("B", (1, 0))),
        call(sti("B", (1, 1))),
        call(sti("C", (0,))),
        call(sti("C", (1,))),
        call(sti("D", (0,))),
        call(sti("D", (1,))),
    ]
    mock.assert_has_calls(calls, any_order=True)
