from io import StringIO
from typing import Callable, List

import pytest

from qosagg.codegen.base_minizinc_generator import MiniZincGeneratorConfig
from qosagg.codegen.categories.category_generator import CategoryGenerator
from qosagg.codegen.naming_strategy import NamingStrategy, PrefixNamingStrategy
from qosagg.domain.attribute import Attribute
from qosagg.domain.category import Category
from qosagg.domain.graph import Task
from qosagg.domain.provision import Provision, AttributeValue
from qosagg.domain.unrolling.unrolled_graph import UGraph, unroll
from qosagg.domain.workflow import Workflow
from tests.helpers import ssg


@pytest.fixture()
def naming() -> NamingStrategy:
    return PrefixNamingStrategy("wf")


@pytest.fixture
def config() -> MiniZincGeneratorConfig:
    return MiniZincGeneratorConfig(output=False, checkpoints=False, reachability=False,
                                   categories=True, taskinst_mapping=True)


GenerateCategoriesFixture = Callable[[Workflow], None]


@pytest.fixture()
def generate_categories(output: StringIO, config: MiniZincGeneratorConfig, naming: NamingStrategy) \
        -> GenerateCategoriesFixture:
    return lambda wf: CategoryGenerator(wf, output, config, naming).generate_categories()


@pytest.mark.parametrize(["num_categories", "expected_sum"], (
        (0, None),
        (1, "wf_cat1_changes"),
        (2, "wf_cat1_changes + wf_cat2_changes")
))
def test_generates_changes_sum_if_categories_exist(num_categories: int, expected_sum: str, output: StringIO,
                                                   generate_categories: GenerateCategoriesFixture):
    # given a workflow with the number of categories that the parameter says we have
    categories: List[Category] = [Category(f"cat{i + 1}") for i in range(num_categories)]
    workflow: Workflow[UGraph] = \
        Workflow(unroll(ssg("A")),
                 [Provision("a", {Task("A")}, {AttributeValue("cat1", "1"), AttributeValue("cat2", "2")})],
                 [], categories, [Attribute("cat1"), Attribute("cat2")])

    # when generating code
    generate_categories(workflow)

    # then there should be the correct sum, or none
    sum_name: str = "wf_all_category_changes"
    actual = output.getvalue()
    print(actual)
    if expected_sum is None:
        assert sum_name not in actual
    else:
        assert f"{sum_name} = {expected_sum};" in actual
