# QosAgg

Parses SoC workflows and generates [MiniZinc](https://www.minizinc.org) code for them, i.a. aggregating QoS attributes.
MiniZinc can then be used to find the best assignment of service providers to tasks and the best path in the workflow.
Hard and optionally also soft constraints (using [MiniBrass](https://isse-augsburg.github.io/minibrass/)) can be added
on top of the generated MiniZinc code for describing what „the best“ service and path selection is.

## Installation

QosAgg requires Python 3.9 or newer.

1. Install the package

   You can either install it using the projects GitLab package registry:
    ```ssh
    pip install qosagg --extra-index-url https://gitlab.com/api/v4/projects/26815888/packages/pypi/simple
    ```

   Or you clone the repository and build the package yourself:
    ```sh
    python3 setup.py bdist_wheel
    pip install dist/*.whl
    ```

2. Execute the help command to get to know the options:

    ```sh
    qosagg --help
    ```

## Usage

You can find more example input with corresponding Minizinc output under [tests/e2e/](tests/e2e). Basic knowledge of
MiniZinc is assumed.

### Input

An input file contains multiple workflow specifications separated by semicolons. Each workflow specifications contains a
workflow graph with tasks, provisions for the tasks in the graph with QoS attributes, and specifications on how to
aggregate QoS attributes. A workflow specification can look like this:

```qosagg
workflow SkateboardProduction {
	graph: Prepare → (Pause + (ProduceWheels | ProduceBoards) + Assemble)^10 → Package;
	
	provision prepareSkyblue for : time = 7.2, cost = 14;
	provision prepareEhDabuEs: time = 8.4, cost = 13;
	provision pause for Pause: time = 0, cost = 0;
	provision yazid for ProduceWheels, ProduceBoards, Assemble: time = 42, cost = 97;
	provision wheelMaster for ProduceWheels: time = 12.9, cost = 26;
	provision boardMakers for ProduceBoards: time = 15, cost = 30;
	provision turboAssembler for Assemble: time = 40.5, cost = 12;
	provision cardboardPacking for Package: time = 12.1, cost = 7;
	provision woodenBoxPacking for Package: time = 19.8, cost = 14;
	
	aggregation time: sum, max;
	attribute cost of int default 0;
	aggregation cost: sum;
};
```

#### Graph

A graph consists of tasks. Task names might occur multiple times in the graph specification. Tasks can be composed
sequentially (`→`/`->`), parallel (`∥`/`|`), as choice where only one of the options is selected (`+`). Moreover, tasks
and compositions can be repeated a fixed number of iterations (`^N` with `N` ∈ ℕ).

Composites can be grouped using parentheses. Sequential composition signs can be omitted, e.g. `A B + C` is equivalent
to `(A → B) + C`.

Use `null` for an empty graph that contains no tasks. You can use `A?` as shortcut for `A + null`.

#### Provisions

For each task, there should be at least one provisioning service. They are specified in the
form `provision <PROVISION_NAME> for <TASK1>, <TASK2>, …: <ATTRIBUTE1_NAME> = <ATTRIBUTE1_VALUE>, …;`. The attribute
value can be nearly any MiniZinc code; QosAgg will try its best to split the attribute array list in such a way that the
result is valid.

Be aware that this might lead to unexpected acceptance of input that you might expect an error
for: `cost = 5, 3 = latency` is interpreted as a single attribute `cost` with value `5, 3 latency`. In return, also
inputs like the following get parsed correctly: `cost = let { int: y = 2} in costTable[SkyBlue, min(x, y)]`.

#### Aggregations

For QoS attributes that should be aggregated along the whole workflow, one has to specify how to do this aggregation for
sequential and parallel composition in the form `aggregation <ATTRIBUTE_NAME>: <SEQENTIAL_FUN>, <PARALLEL_FUN>;`. The
sequential and parallel aggregation functions are given as names of MiniZinc functions. These can be predefined, like
MiniZinc‘s `sum` or `max`, but you can also reference your own MiniZinc function, as long as you add them to the
MiniZinc code later on.

There is no need for an aggregation for loops and choices: loops are unfolded and handled like a sequential composition
of the loop iterations, and as MiniZinc decides for a specific path in the workflow graph, aggregation only cares about
the selected option of every choice composition.

#### Attributes

Attributes are added implicitly when you use them in aggregations and when you provide values for them for at least one
provision. You can also add attributes explicitly: `attribute <NAME> of <TYPE> default <VALUE>;`.

The type specification again can be nearly any MiniZinc code, e.g. variable types like `var int`. However, this can be
limited to numbers and identifiers using a command-line-flag in order to speed up parsing.

If the type is omitted, `float` is used. The default value also is optional and is used wherever the attribute is not
explicitly specified for a provision.

#### Categories

Attributes can optionally be marked as categories by adding `category <ATTRIBUTE_NAME>;`. A common use case is to have a
provider attribute marked as category, and use the generated variables to prevent provider hopping.

#### MiniZinc blocks

One can add MiniZinc code before, in between and after workflows inside a `minizinc` block:

```qosagg
workflow { … };

minizinc {
   % any minizinc code
}
```

The entered code will be copied to the output file as it is. This allows to put everything into a single file when there
is not much additional MiniZinc code.

### Output

The output is a MiniZinc file containing several decision variables, declarations and output statements. Some of these
can be disabled using command line flags; use `–-help`  to find out more.

All identifiers are prefixed with the workflow name and an underscore, or `wf_` if no name is given.

#### General declarations

Enumerations are generated for the specified tasks and provisions. Another enumeration contains at least one task
instance per task; more that one for tasks inside loops. There are mappings between tasks and task instances.

#### Decision variables

There is one array `selection` mapping task instances to provisions. For each choice composition and, if inside one or
more loops, for each loop iteration, a decision variable is created. For example the workflow `A + B` would lead to a
decision variable `choice1`, that has value `1` if `A` is executed and `2` if `B` is executed.

#### Aggregation

For the QoS attributes with aggregation functions, aggregations are generated, named `aggregated_<ATTRIBUTE_NAME>`.

#### Checkpoints and reachability

Some usecases require aggregations not only for the whole workflow, but for intermediate steps already, especially when
using non-monotone attributes. If for example provisions can produce but also consume resources, it often is necessary
to ensure that at no point in time more resources are in use than have been produced before. Ensuring that the overall
balance at the end is positive is not enough.

In these situations, checkpoints come in handy. There is a checkpoint generated directly before and after every task
instance. For each checkpoint, the attribute aggregations are calculated up to that point. Additionaly, a mapping is
created from checkpoints to task instances.

Also a set containing all checkpoints is generated and can be used for specifying invariants. This set however is not
just a static collection, but adapts to the path selection that MiniZinc does. Checkpoints that are not reachable
because they are not part of the selected path will not be included in the checkpoints set. The reachability might also
be used for custom use cases.

#### Categories

For categories, a few predicates and functions are generated that help you detect whenever the categories of the
provisions for the instances of the same task change. As summary, the category changes between instances of the same
task are counted per category and in total. When reachability is enabled, non-reachable instances are ignored. More
complex cases, e.g. counting category changes between instances of two dependent tasks, can not be covered automatically
because it is not clear how the tasks depend, but you can use the generated functions to count the number of category
changes for your custom array of task instances.

#### Output statements

Two output statements are added by default. The first one prints the selected path through the workflow with the
selected provisions for the solution that MiniZinc finds. The second one prints the aggregations for the workflow QoS.
In case categories are enabled and provided, a third output statement with a summary about the numbers of category
changes is added as well.

## Development

1. Clone the repository
2. Install the package in development mode:

    ```sh
    pip install -e .
    ```

3. Download ANTLR and build the parsers:
   ```sh
   wget --no-verbose https://www.antlr.org/download/antlr-4.10.1-complete.jar
   cd qosagg/parsing
   ../../scripts/generate_parser.sh ../../antlr-4.10.1-complete.jar
   ```

4. Set up and try the testing:

    ```sh
    sudo apt install minizinc
    pip install tox
    tox
    ```
   
   The E2E-Tests require MiniZinc to be installed and available as `minizinc`.
   You can pass a customized path for the executable by setting the `MINIZINCBIN` environment variable and making Tox 
   forward it to the test. For example:
   ```sh
   MINIZINCBIN=../minizinc.sh TOX_TESTENV_PASSENV=MINIZINCBIN tox
   ```

5. Start developing 😄