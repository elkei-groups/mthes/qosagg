from qosagg.domain.graph import Task
from qosagg.parsing.task_occurrences_tracer import TaskOccurrencesTracer


def test_get_next_occurrence() -> None:
    # given a tracer and some tasks
    tracer = TaskOccurrencesTracer()
    task_a = Task("A")
    task_b = Task("B")

    # when adding occurrences, the occurrences should be adapted
    a0 = tracer.get_next_task_occurrence(task_a)
    assert a0.is_sole_task_occurrence
    assert a0.task == task_a
    a1 = tracer.get_next_task_occurrence(task_a)
    assert not a0.is_sole_task_occurrence
    assert a0.occurrence == 0
    assert a1.occurrence == 1
    assert a1.task == task_a
    b0 = tracer.get_next_task_occurrence(task_b)
    assert b0.is_sole_task_occurrence
    assert not a1.is_sole_task_occurrence
    assert not a0.is_sole_task_occurrence
