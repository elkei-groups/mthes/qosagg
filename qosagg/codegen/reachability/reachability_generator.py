from typing import TextIO

from qosagg.codegen.base_minizinc_generator import BaseMiniZincGenerator, MiniZincGeneratorConfig
from qosagg.codegen.graph_collector import GraphCollector
from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.codegen.reachability.reachability_graph_visitor import ReachabilityGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import UGraph


class ReachabilityGenerator(BaseMiniZincGenerator):
    def __init__(self, graph: UGraph, file: TextIO, config: MiniZincGeneratorConfig, naming: NamingStrategy,
                 collector: GraphCollector):
        super().__init__(file, config, naming)
        self.graph = graph
        self.collector = collector

    def generate_reachability(self) -> None:
        self._generate_reachability_assignments()
        self._generate_reachability_functions()

    def _generate_reachability_assignments(self):
        visitor = ReachabilityGraphVisitor(self.naming)
        visitor.visit(self.graph)
        values = [visitor.reachabilities[task_inst] for task_inst in self.collector.task_instances]
        self.generate_array(self.naming.name_task_instance_enum(), self.naming.name_reachability_array(), values,
                            array_type="bool", variable=True)

    def _generate_reachability_functions(self):
        reach_array = self.naming.name_reachability_array()
        task_type = self.naming.name_task_enum()
        tasks_function = self.naming.name_reachable_tasks_function()
        task_inst_type = self.naming.name_task_instance_enum()
        task_insts_function = self.naming.name_reachable_task_instances_function()
        task_inst_task_map = self.naming.name_task_inst_task_map()
        self.file.write(f"function var set of {task_type}: {tasks_function}(set of {task_type}: ts) =\n"
                        f"\t{{ t | t in ts where exists(ti in {task_inst_type})("
                        f"{task_inst_task_map}[ti] == t ∧ {reach_array}[ti]"
                        f") }};\n")
        self.file.write(f"function var set of {task_inst_type}: {task_insts_function}(set of {task_inst_type}: tis) =\n"
                        f"\t{{ ti | ti in tis where {reach_array}[ti] }};\n")
        if self.config.checkpoints:
            cp_task_inst_map = self.naming.name_checkpoint_task_inst_map()
            cp_type = self.naming.name_checkpoint_enum()
            cp_function = self.naming.name_reachable_checkpoints_function()
            self.file.write(f"function var set of {cp_type}: {cp_function}(set of {cp_type}: cps) =\n"
                            f"\t{{ cp | cp in cps where {reach_array}[{cp_task_inst_map}[cp]] }};\n")
