import textwrap
from io import StringIO
from typing import Callable

import pytest

from qosagg.codegen.base_minizinc_generator import MiniZincGeneratorConfig
from qosagg.codegen.naming_strategy import PrefixNamingStrategy
from qosagg.codegen.output.output_generator import OutputGenerator
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.category import Category
from qosagg.domain.graph import CompositeGraph, GraphComposition, LoopedGraph, Task
from qosagg.domain.provision import Provision, AttributeValue
from qosagg.domain.unrolling.unrolled_graph import unroll
from qosagg.domain.workflow import NamedWorkflow, Workflow
from tests.helpers import ssg


@pytest.fixture()
def config() -> MiniZincGeneratorConfig:
    return MiniZincGeneratorConfig(output=True, checkpoints=False, reachability=False)


@pytest.fixture()
def generate(output: StringIO, config: MiniZincGeneratorConfig) \
        -> Callable[[NamedWorkflow], None]:
    return lambda w: OutputGenerator(w, output, config, PrefixNamingStrategy("wf")) \
        .generate_output()


def test_output_generation(output: StringIO, generate: Callable[[Workflow], None]):
    # given a valid workflow (A + B) | C D
    choice1 = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.CHOICE, "choice1")
    workflow = NamedWorkflow("wf", unroll(CompositeGraph([
        choice1,
        CompositeGraph([ssg("C"), ssg("D")], GraphComposition.SEQUENTIAL, "s1"),
    ], GraphComposition.PARALLEL, "p1")),
                             [
                                 Provision("p1", {Task("A"), Task("B"), Task("C"), Task("D")}, {
                                     AttributeValue("cost", "1.0")
                                 }),
                                 Provision("p2", {Task("A"), Task("B"), Task("C"), Task("D")}, {
                                     AttributeValue("cost", "2.0")
                                 })
                             ], [Aggregation("cost", "sum", "sum")], [], [])

    # when generating the MiniZinc code
    generate(workflow)

    # then the generator should have generated graph and aggregation outputs
    expectation = textwrap.dedent("""\
        array[WF_PROVISION] of string: wf_provision_names = ["p1", "p2"];
        output ["Selection graph for wf:\\n\\t", "(", if fix(wf_choice1) == 1 then "A=" else "" endif, if fix(wf_choice1) == 1 then wf_provision_names[fix(wf_selection[wf_A_inst])] else "" endif, if fix(wf_choice1) == 2 then "B=" else "" endif, if fix(wf_choice1) == 2 then wf_provision_names[fix(wf_selection[wf_B_inst])] else "" endif, " | ", "C=", wf_provision_names[fix(wf_selection[wf_C_inst])], " → ", "D=", wf_provision_names[fix(wf_selection[wf_D_inst])], ")", "\\n"];
        output ["Aggregations for wf:\\n",
        \t"\\tcost: \\(wf_aggregated_cost)\\n"];
        """).strip()  # noqa: W293, E501
    actual = output.getvalue()
    print(actual)
    assert expectation in actual


def test_loop_output_generation(output: StringIO, generate: Callable[[Workflow], None]):
    # given a looped workflow A^2
    workflow = NamedWorkflow("wf", unroll(LoopedGraph(ssg("A"), 2, "l1")), [], [], [], [])

    # when generating the MiniZinc code
    generate(workflow)

    # then the generator should have generated a proper output string for the loop
    expectation = textwrap.dedent("""\
        output ["Selection graph for wf:\\n\\t", "A=", wf_provision_names[fix(wf_selection[wf_A_inst_1])], " → ", "A=", wf_provision_names[fix(wf_selection[wf_A_inst_2])], "\\n"];
        """).strip()  # noqa: E501
    print(output.getvalue())
    assert expectation in output.getvalue()


def test_looped_choice_output_generation(output: StringIO, generate: Callable[[Workflow], None]):
    # given a looped workflow (A + B)^2
    choice1 = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.CHOICE, "choice1")
    workflow = NamedWorkflow("wf", unroll(LoopedGraph(choice1, 2, "l1")), [], [], [], [])

    # when generating the MiniZinc code
    generate(workflow)

    # then the generator should have generated a proper output string for the loop
    expectation = textwrap.dedent("""\
        output ["Selection graph for wf:\\n\\t", if fix(wf_choice1_1) == 1 then "A=" else "" endif, if fix(wf_choice1_1) == 1 then wf_provision_names[fix(wf_selection[wf_A_inst_1])] else "" endif, if fix(wf_choice1_1) == 2 then "B=" else "" endif, if fix(wf_choice1_1) == 2 then wf_provision_names[fix(wf_selection[wf_B_inst_1])] else "" endif, " → ", if fix(wf_choice1_2) == 1 then "A=" else "" endif, if fix(wf_choice1_2) == 1 then wf_provision_names[fix(wf_selection[wf_A_inst_2])] else "" endif, if fix(wf_choice1_2) == 2 then "B=" else "" endif, if fix(wf_choice1_2) == 2 then wf_provision_names[fix(wf_selection[wf_B_inst_2])] else "" endif, "\\n"];
        """).strip()  # noqa: E501
    actual = output.getvalue()
    print(actual)
    assert expectation in actual


def test_zero_loop(output: StringIO, generate: Callable[[Workflow], None]):
    # given a looped workflow A^0
    workflow = NamedWorkflow("wf", unroll(LoopedGraph(ssg("A"), 0, "l1")), [], [], [], [])

    # when generating the MiniZinc code
    generate(workflow)

    # then the generator should have generated a proper output string for the loop
    expectation = textwrap.dedent("""\
        output ["Selection graph for wf:\\n\\t", "\\n"];
        """).strip()  # noqa: E501
    actual = output.getvalue()
    print(actual)
    assert expectation in actual


def test_output_for_categories(output: StringIO, generate: Callable[[Workflow], None], config: MiniZincGeneratorConfig):
    # given a simple workflow
    workflow = NamedWorkflow("wf", unroll(LoopedGraph(ssg("A"), 3, "l1")), [], [],
                             [Category("provider"), Category("cat2")], [])

    # when generating the MiniZinc code
    config.categories = True
    generate(workflow)

    # then the generator should have generated a summary of category changes
    expectation = textwrap.dedent("""\
    output ["Category changes for wf:\\n",
    \t"\\tprovider: \\(wf_provider_changes)\\n",
    \t"\\tcat2: \\(wf_cat2_changes)\\n",
    \t"\\tΣ changes: \\(wf_all_category_changes)\\n"];
    """).strip()
    actual = output.getvalue()
    print(actual)
    assert expectation in actual
