import textwrap
from io import StringIO
from typing import TextIO, Optional, Union, Iterable

import pytest

from qosagg.codegen.base_minizinc_generator import MiniZincGeneratorConfig, BaseMiniZincGenerator, MznParam
from qosagg.codegen.naming_strategy import PrefixNamingStrategy


@pytest.fixture
def config() -> MiniZincGeneratorConfig:
    return MiniZincGeneratorConfig()


@pytest.fixture
def generator(output: TextIO, config: MiniZincGeneratorConfig) -> BaseMiniZincGenerator:
    naming = PrefixNamingStrategy("wf")
    return BaseMiniZincGenerator(output, config, naming)


def test_generate_enum(generator: BaseMiniZincGenerator, output: StringIO) -> None:
    # when generating a simple enum with 3 values
    generator.generate_enum("enumName", ("val1", "val2, val3"))

    # then the output should be exactly as follows
    actual = output.getvalue().strip()
    assert actual == "enum enumName = { val1, val2, val3 };"


@pytest.mark.parametrize(("values", "array_type", "variable", "expectation"), (
        (None, "float", False, "array[1..n] of float: arrName;"),
        (None, "float", True, "array[1..n] of var float: arrName;"),
        (None, "var float", True, "array[1..n] of var float: arrName;"),
        (None, "var float", False, "array[1..n] of var float: arrName;"),
        ("otherArray", "1..10", True, "array[1..n] of var 1..10: arrName = otherArray;"),
        (range(6), "int", False, "array[1..n] of int: arrName = [0, 1, 2, 3, 4, 5];")
))
def test_generate_array(generator: BaseMiniZincGenerator, output: StringIO, values: Optional[Union[Iterable, str]],
                        array_type: str, variable: bool, expectation: str) -> None:
    # when generating an array
    generator.generate_array("1..n", "arrName", values, array_type, variable)

    # then the output should exactly match the expectation
    actual = output.getvalue().strip()
    assert actual == expectation


def test_generate_constraint(generator: BaseMiniZincGenerator, output: StringIO) -> None:
    generator.generate_constraint("myArray[myIndex] <= floor(5.5)")
    assert output.getvalue().strip() == "constraint myArray[myIndex] <= floor(5.5);"


@pytest.mark.parametrize(("var_type", "variable", "expectation"), (
        ("int", False, "int: myVar = 17;"),
        ("int", True, "var int: myVar = 17;"),
        ("var int", False, "var int: myVar = 17;"),
        ("var int", True, "var int: myVar = 17;"),
))
def test_generate_var(generator: BaseMiniZincGenerator, output: StringIO, var_type: str,
                      variable: bool, expectation: str) -> None:
    generator.generate_var("myVar", "17", var_type, variable)
    assert output.getvalue().strip() == expectation


@pytest.mark.parametrize(("value", "var_set", "expectation"), (
        ("otherSet", False, "set of int: mySet = otherSet;"),
        ("otherSet", True, "var set of int: mySet = otherSet;"),
        (range(5), False, "set of int: mySet = { 0, 1, 2, 3, 4 };")
))
def test_generate_set(generator: BaseMiniZincGenerator, output: StringIO, value: Union[str, Iterable[str]],
                      var_set: bool, expectation: str) -> None:
    generator.generate_set("mySet", value, "int", var_set)
    assert output.getvalue().strip() == expectation


def test_generate_predicate(generator: BaseMiniZincGenerator, output: StringIO) -> None:
    generator.generate_predicate("myPredicate", [MznParam("p1", "EnumVal"), MznParam("p2", "var 0.0..10.0")],
                                 "let {\n\tvar int: i;\n\tconstraint p2 <= i ∧ i < p2 + 1;\n} in i == fun(p1)")
    actual = output.getvalue().strip()
    expectation = textwrap.dedent("""\
        predicate myPredicate(EnumVal: p1, var 0.0..10.0: p2) =
        \tlet {
        \t\tvar int: i;
        \t\tconstraint p2 <= i ∧ i < p2 + 1;
        \t} in i == fun(p1);""")
    assert actual == expectation


def test_generate_function(generator: BaseMiniZincGenerator, output: StringIO) -> None:
    generator.generate_function("myFun", "int", [MznParam("p1", "EnumVal"), MznParam("p2", "var 0.0..10.0")],
                                "let {\n\tvar int: i;\n\tconstraint p2 <= i ∧ i < p2 + 1;\n} in i")
    actual = output.getvalue().strip()
    expectation = textwrap.dedent("""\
        function int: myFun(EnumVal: p1, var 0.0..10.0: p2) =
        \tlet {
        \t\tvar int: i;
        \t\tconstraint p2 <= i ∧ i < p2 + 1;
        \t} in i;""")
    assert actual == expectation


def test_is_variable_type() -> None:
    assert not BaseMiniZincGenerator.is_variable_type("float")
    assert BaseMiniZincGenerator.is_variable_type("var float")
    assert BaseMiniZincGenerator.is_variable_type("array[int] of var float")
    assert not BaseMiniZincGenerator.is_variable_type("variance")
    assert not BaseMiniZincGenerator.is_variable_type("invar")
