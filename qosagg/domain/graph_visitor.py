from abc import ABC, abstractmethod
from typing import Generic, TypeVar, TYPE_CHECKING

if TYPE_CHECKING:
    from qosagg.domain.unrolling.unrolled_graph import CompositeUGraph, TaskInstance, UGraph

_T = TypeVar("_T")


class UGraphVisitor(ABC, Generic[_T]):
    def visit(self, ugraph: "UGraph") -> _T:
        return ugraph.accept(self)

    @abstractmethod
    def visit_task_instance(self, task_inst: "TaskInstance") -> _T:
        pass

    @abstractmethod
    def visit_composite_ugraph(self, ugraph: "CompositeUGraph") -> _T:
        pass
