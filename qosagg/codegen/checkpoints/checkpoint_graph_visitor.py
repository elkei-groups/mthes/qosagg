from typing import List, TextIO, Iterable

from qosagg.codegen.aggregating_graph_visitor import AggregatingGraphVisitor
from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.graph import GraphComposition
from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling.task_instance import CheckpointType, Checkpoint
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph, UGraph


class CheckpointGraphVisitor(UGraphVisitor[None]):
    def __init__(self, aggregation: Aggregation, naming: NamingStrategy, file: TextIO):
        self.aggregation = aggregation
        self.naming = naming
        self.file = file
        self.sequence_stack: List[str] = []

    def visit_task_instance(self, task_inst: TaskInstance) -> None:
        seq_agg = self.aggregation.sequential_aggregator
        name_before = self.naming.name_checkpoint(Checkpoint(task_inst, CheckpointType.PRE))
        name_after = self.naming.name_checkpoint(Checkpoint(task_inst, CheckpointType.POST))
        array = self.naming.name_checkpoint_array(self.aggregation.attribute_name)
        value_before = f"{seq_agg}([{', '.join(self.sequence_stack)}])"
        task_value = self.naming.name_selection_attribute(task_inst, self.aggregation.attribute_name)
        value_after = f"{seq_agg}([{array}[{name_before}], {task_value}])"
        for name, value in ((name_before, value_before), (name_after, value_after)):
            self.file.write(f"constraint {array}[{name}] = {value};\n")

    def visit_composite_ugraph(self, composite: CompositeUGraph) -> None:
        if composite.composition == GraphComposition.SEQUENTIAL:
            self.visit_sequential_composition(composite.subgraphs)
        else:
            self.visit_non_sequential_composition(composite.subgraphs)

    def visit_sequential_composition(self, subgraphs: Iterable[UGraph]) -> None:
        if not subgraphs:
            return
        count = 0
        for subgraph in subgraphs:
            count += 1
            self.visit(subgraph)
            self.sequence_stack.append(self._get_aggregator().visit(subgraph))
        del self.sequence_stack[-count:]

    def visit_non_sequential_composition(self, subgraphs: Iterable[UGraph]) -> None:
        for subgraph in subgraphs:
            self.visit(subgraph)

    def _get_aggregator(self) -> AggregatingGraphVisitor:
        return AggregatingGraphVisitor(self.aggregation, self.naming)
