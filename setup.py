from setuptools import setup, find_packages

with open("README.md") as f:
    readmeText = f.read()

with open("VERSION") as f:
    version = f.read()

setup(
    name='qosagg',
    version=version,
    description='Parses SoC workflows and aggregates the QoS attributes',
    long_description=readmeText,
    long_description_content_type="text/markdown",
    license_files=['LICENSE'],
    author='Elias Keis',
    author_email='git-commits@elkei.de',
    url='https://gitlab.com/elkei-groups/mthes/qosagg',
    packages=find_packages(exclude=['tests', 'tests.*', 'docs', 'lazy']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['qosagg=qosagg.app:run'],
    },
    install_requires=[
        'antlr4-python3-runtime',
        'lazy'
    ],
    tests_require=[
        'pytest',
        'pytest-mock'
    ]
)
