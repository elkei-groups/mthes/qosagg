from typing import TextIO, List

from qosagg.codegen.base_minizinc_generator import BaseMiniZincGenerator, MiniZincGeneratorConfig
from qosagg.codegen.checkpoints.checkpoint_graph_visitor import CheckpointGraphVisitor
from qosagg.codegen.graph_collector import GraphCollector
from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.unrolling.task_instance import CheckpointType, Checkpoint
from qosagg.domain.unrolling.unrolled_graph import UGraph
from qosagg.domain.workflow import Workflow


class CheckpointGenerator(BaseMiniZincGenerator):
    def __init__(self, workflow: Workflow[UGraph], file: TextIO, config: MiniZincGeneratorConfig,
                 naming: NamingStrategy, graph_collector: GraphCollector):
        super().__init__(file, config, naming)
        self._workflow = workflow
        self._graph_collector = graph_collector

    def generate_checkpoints(self):
        self._generate_cp_enum()
        self._generate_all_cp_set()
        self._generate_checkpoint_task_inst_relation()
        self._generate_arrays()
        self._generate_sets()
        self._generate_aggregations()

    def _generate_cp_enum(self) -> None:
        all_names = [self.naming.name_checkpoint(cp) for cp in self._graph_collector.checkpoints]
        self.generate_enum(self.naming.name_checkpoint_enum(), all_names)

    def _generate_all_cp_set(self) -> None:
        name = self.naming.name_all_checkpoints_set()
        enum = self.naming.name_checkpoint_enum()
        if self.config.reachability:
            reachable_fun = self.naming.name_reachable_checkpoints_function()
            value = f"{reachable_fun}({enum})"
            self.generate_set(name, value, enum, var_set=True)
        else:
            self.generate_set(name, enum, enum, var_set=False)

    def _generate_checkpoint_task_inst_relation(self) -> None:
        values = [self.naming.name_task_instance(cp.task_instance) for cp in self._graph_collector.checkpoints]
        self.generate_array(self.naming.name_checkpoint_enum(), self.naming.name_checkpoint_task_inst_map(), values,
                            array_type=self.naming.name_task_instance_enum())

    def _generate_arrays(self) -> None:
        for agg in sorted(self._workflow.aggregations, key=lambda a: a.attribute_name):
            self.generate_array(self.naming.name_checkpoint_enum(),
                                self.naming.name_checkpoint_array(agg.attribute_name),
                                array_type=self._workflow.attributes_by_name[agg.attribute_name].value_type,
                                variable=True)

    def _generate_sets(self) -> None:
        cp_enum = self.naming.name_checkpoint_enum()
        grouped_task_instances = self._graph_collector.task_instances_by_task
        for (task, instances) in grouped_task_instances.items():
            for cp_type in CheckpointType:
                name = self.naming.name_checkpoint_set(task, cp_type) if not self.config.reachability \
                    else self.naming.name_checkpoint_set_unfiltered(task, cp_type)
                cp_names: List[str] = [self.naming.name_checkpoint(Checkpoint(task_inst, cp_type))
                                       for task_inst in instances]
                self.generate_set(name, cp_names, cp_enum)
                if self.config.reachability:
                    name_filtered = self.naming.name_checkpoint_set(task, cp_type)
                    filter_fun = self.naming.name_reachable_checkpoints_function()
                    self.generate_set(name_filtered, f"{filter_fun}({name})", cp_enum, var_set=True)

    def _generate_aggregations(self) -> None:
        for aggregation in self._workflow.aggregations:
            self._generate_aggregation(aggregation)

    def _generate_aggregation(self, aggregation: Aggregation) -> None:
        visitor = CheckpointGraphVisitor(aggregation, self.naming, self.file)
        visitor.visit(self._workflow.graph)
