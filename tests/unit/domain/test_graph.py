import pytest

from qosagg.domain.graph import LoopedGraph, CompositeGraph, GraphComposition, TaskOccurrence, Task
from tests.helpers import ssg


@pytest.mark.parametrize("composite_type", list(GraphComposition))
def test_composite_graph_eq(composite_type: GraphComposition) -> None:
    # given two equal composite graphs
    c1 = CompositeGraph([ssg("A"), ssg("B")], composite_type, "comp")
    c2 = CompositeGraph([ssg("A"), ssg("B")], composite_type, "comp")

    # then they should be equal
    assert c1 == c2


def test_composite_graph_neq() -> None:
    # given composite graphs not equal
    c = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.PARALLEL, "comp")
    c_sub = CompositeGraph([ssg("B"), ssg("A")], GraphComposition.PARALLEL, "comp")
    c_type = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.CHOICE, "comp")
    c_name = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.PARALLEL, "comp2")

    # then they should not be equal
    assert c != c_sub
    assert c != c_type
    assert c != c_name
    assert c != "foobar"


@pytest.mark.parametrize(["composition", "exp_sign"], [
    (GraphComposition.PARALLEL, "|"),
    (GraphComposition.CHOICE, "+"),
    (GraphComposition.SEQUENTIAL, "→"),
])
def test_composite_graph_str(composition: GraphComposition, exp_sign: str) -> None:
    c = CompositeGraph([ssg("A"), ssg("B")], composition, "comp")
    assert str(c) == f"(A {exp_sign} B)=comp"


def test_looped_graph_eq() -> None:
    # given two eq graphs
    l1 = LoopedGraph(ssg("t"), 3, "loop")
    l2 = LoopedGraph(ssg("t"), 3, "loop")

    # then they should be equal
    assert l1 == l2


def test_looped_graph_neq() -> None:
    # given two neq graphs
    loop = LoopedGraph(ssg("t"), 3, "loop")
    l_subgraph = LoopedGraph(ssg("t2"), 3, "loop")
    l_count = LoopedGraph(ssg("t2"), 2, "loop")
    l_name = LoopedGraph(ssg("t2"), 3, "loop2")

    # then they should not be equal
    assert loop != l_subgraph
    assert loop != l_count
    assert loop != l_name
    assert loop != "foobar"


def test_looped_graph_str() -> None:
    # given a looped graph
    loop = LoopedGraph(ssg("t"), 3, "loop1")

    assert str(loop) == "(t)^3=loop1"


def test_task_occurrence() -> None:
    to = TaskOccurrence(Task("Task"))
    assert to.occurrence is None
    assert to.is_sole_task_occurrence
    to.is_sole_task_occurrence = False
    assert not to.is_sole_task_occurrence
    assert to.occurrence == 0
    to.is_sole_task_occurrence = True
    assert to.occurrence is None
    to.occurrence = 3
    assert not to.is_sole_task_occurrence
