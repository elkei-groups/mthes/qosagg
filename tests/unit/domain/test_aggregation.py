from qosagg.domain.aggregation import Aggregation


def test_aggregation_eq():
    # given two equal aggregations
    a1 = Aggregation("attr", "foo", "bar")
    a2 = Aggregation("attr", "foo", "bar")

    # then they should equal
    assert a1 == a2


def test_aggregation_neq():
    # given two non-equal aggregations
    a = Aggregation("attr", "foo", "bar")
    a_attr = Aggregation("attr2", "foo", "bar")
    a_seq = Aggregation("attr", "foo2", "bar")
    a_par = Aggregation("attr", "foo", "bar2")

    # then they should not equal
    assert a != a_attr
    assert a != a_seq
    assert a != a_par
    assert a != "foobar"


def test_aggregation_str():
    a = Aggregation("attr", "foo", "bar")
    assert str(a) == "aggregation attr: foo, bar"
