import textwrap
from typing import TextIO

from qosagg.codegen.base_minizinc_generator import BaseMiniZincGenerator, MiniZincGeneratorConfig, MznParam
from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.category import Category
from qosagg.domain.unrolling.unrolled_graph import UGraph
from qosagg.domain.workflow import Workflow


class CategoryGenerator(BaseMiniZincGenerator):

    def __init__(self, workflow: Workflow[UGraph], file: TextIO, config: MiniZincGeneratorConfig,
                 naming: NamingStrategy):
        super().__init__(file, config, naming)
        self._workflow = workflow

    def generate_categories(self) -> None:
        self._generate_is_index_of_predicate()
        self._generate_is_predecessor_predicate()
        self._generate_category_changed_predicate()
        self._generate_count_category_changes_function()
        self._generate_count_category_changes_within_task_function()

        for category in self._workflow.categories:
            self._generate_category_changes(category)
        self._generate_all_category_changes_sum()

    def _generate_is_index_of_predicate(self):
        # TODO this is independent of the workflow and should only be generated once per file
        self.generate_predicate(self.naming.name_is_index_of_predicate(),
                                [MznParam("idx", "var int"), MznParam("elem", "var $T"),
                                 MznParam("arr", "array[int] of var opt $T")],
                                "exists(i in index_set(arr)) (arr[i] = elem /\\ i = idx)")

    def _generate_is_predecessor_predicate(self):
        is_index_of_predicate = self.naming.name_is_index_of_predicate()
        task_inst_enum = self.naming.name_task_instance_enum()
        params = [MznParam("previous", f"var {task_inst_enum}"),
                  MznParam("current", f"var {task_inst_enum}"),
                  MznParam("arr", f"array[int] of var opt {task_inst_enum}")]
        body = textwrap.dedent(f"""\
            exists(i, j in index_set(arr) where i < j) (
                {is_index_of_predicate}(i, previous, arr) /\\
                {is_index_of_predicate}(j, current, arr) /\\
                forall(k in index_set(arr) where i < k /\\ k < j) (absent(arr[k]))
            )""")
        self.generate_predicate(self.naming.name_task_inst_is_predecessor_predicate(), params, body)

    def _generate_category_changed_predicate(self):
        task_inst_enum = self.naming.name_task_instance_enum()
        provision_enum = self.naming.name_provision_enum()
        selection_array = self.naming.name_selection_array()
        is_predecessor_predicate = self.naming.name_task_inst_is_predecessor_predicate()
        params = [MznParam("inst", f"var opt {task_inst_enum}"),
                  MznParam("attr", f"array[{provision_enum}] of $T"),
                  MznParam("insts", f"array[int] of var opt {task_inst_enum}")]
        body = textwrap.dedent(f"""\
            exists(previous in insts)(
            \toccurs(inst) /\\ occurs(previous) /\\
            \t{is_predecessor_predicate}(deopt(previous), deopt(inst), insts) /\\
            \tattr[{selection_array}[inst]] != attr[{selection_array}[previous]]
            )""")
        self.generate_predicate(self.naming.name_category_changed_predicate(), params, body)

    def _generate_count_category_changes_function(self):
        task_inst_enum = self.naming.name_task_instance_enum()
        provision_enum = self.naming.name_provision_enum()
        category_changed_predicate = self.naming.name_category_changed_predicate()
        params = [MznParam("attr", f"array[{provision_enum}] of $T"),
                  MznParam("insts", f"array[int] of {task_inst_enum}")]
        if self.config.reachability:
            reachable = self.naming.name_reachability_array()
            body = textwrap.dedent(f"""\
                let {{
                \tarray[int] of var opt {task_inst_enum}: reachable_insts = [ inst | inst in insts where {reachable}[inst]];
                }} in
                sum(inst in reachable_insts)(
                \tbool2int({category_changed_predicate}(inst, attr, reachable_insts))
                )""")  # noqa: E501
        else:
            body = textwrap.dedent(f"""\
                sum(inst in insts)(
                \tbool2int({category_changed_predicate}(inst, attr, insts))
                )""")
        self.generate_function(self.naming.name_count_category_changes_function(), "var int", params, body)

    def _generate_count_category_changes_within_task_function(self):
        task_inst_enum = self.naming.name_task_instance_enum()
        task_of_inst_mapping = self.naming.name_task_inst_task_map()
        provision_enum = self.naming.name_provision_enum()
        task_enum = self.naming.name_task_enum()
        count_function = self.naming.name_count_category_changes_function()
        params = [MznParam("attr", f"array[{provision_enum}] of $T"),
                  MznParam("insts", f"array[int] of {task_inst_enum}")]
        body = textwrap.dedent(f"""\
            sum(task in {task_enum})(
            \t{count_function}(attr, [inst | inst in insts where {task_of_inst_mapping}[inst] == task])
            )""")
        self.generate_function(self.naming.name_count_category_changes_within_task_function(), "var int", params, body)

    def _generate_category_changes(self, category: Category) -> None:
        name = self.naming.name_category_changes_variable(category.attribute_name)
        count_function = self.naming.name_count_category_changes_within_task_function()
        attribute = self.naming.name_attribute_array(category.attribute_name)
        task_insts = self.naming.name_task_instance_enum()
        self.generate_var(name, f"{count_function}({attribute}, {task_insts})", "int", variable=True)

    def _generate_all_category_changes_sum(self) -> None:
        if not self._workflow.categories:
            return
        name = self.naming.name_all_category_changes_variable()
        summands = (self.naming.name_category_changes_variable(category.attribute_name) for category in
                    self._workflow.categories)
        self.generate_var(name, " + ".join(summands), "int", variable=True)
