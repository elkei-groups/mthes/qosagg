from abc import ABC, abstractmethod
from typing import Set, TypeVar, Generic, Sequence

from qosagg.domain.graph import SingletonGraph, Task, CompositeGraph, Graph, LoopedGraph, GraphComposition
from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling._composite_unrolling_sequence import _CompositeUnrollingSequence
from qosagg.domain.unrolling._loop_unrolling_sequence import _LoopUnrollingSequence
from qosagg.domain.unrolling.task_instance import Iteration, TaskInstance

_G = TypeVar("_G", bound=Graph)
_T = TypeVar("_T")


class UGraph(ABC, Generic[_G]):
    def __init__(self, graph: _G, iteration: Iteration):
        self._graph: _G = graph
        self.iteration = iteration

    def get_tasks(self) -> Set[Task]:
        return self._graph.get_tasks()

    @abstractmethod
    def accept(self, visitor) -> _T:
        pass


def unroll(graph: Graph, iteration: Iteration = None) -> UGraph:
    if iteration is None:
        iteration = ()
    if isinstance(graph, SingletonGraph):
        return SingletonUGraph(graph, iteration)
    elif isinstance(graph, CompositeGraph):
        return ClassicCompositeUGraph(graph, iteration)
    elif isinstance(graph, LoopedGraph):
        return LoopBasedCompositeUGraph(graph, iteration)
    else:
        raise TypeError(f"Can not handle subgraph of type {type(graph)}")


class SingletonUGraph(UGraph[SingletonGraph]):
    def __init__(self, graph: SingletonGraph, iteration: Iteration) -> None:
        super().__init__(graph, iteration)

    def _get_task_instance(self) -> TaskInstance:
        return TaskInstance(self._graph.task_occ, self.iteration)

    task_instance = property(_get_task_instance)

    def accept(self, visitor: UGraphVisitor[_T]) -> _T:
        return visitor.visit_task_instance(self.task_instance)


class CompositeUGraph(UGraph[_G], ABC):
    def __init__(self, graph: _G, iteration: Iteration):
        super().__init__(graph, iteration)

    @property
    @abstractmethod
    def subgraphs(self) -> Sequence[UGraph]:
        pass

    @property
    @abstractmethod
    def composition(self) -> GraphComposition:
        pass

    @property
    @abstractmethod
    def name(self) -> str:
        pass

    def accept(self, visitor: UGraphVisitor[_T]) -> _T:
        return visitor.visit_composite_ugraph(self)


class ClassicCompositeUGraph(CompositeUGraph[CompositeGraph]):
    def __init__(self, graph: CompositeGraph, iteration: Iteration) -> None:
        super().__init__(graph, iteration)
        self._graph = graph

    @property
    def subgraphs(self) -> Sequence[UGraph]:
        return _CompositeUnrollingSequence(self._graph.subgraphs, self.iteration, unroll)

    @property
    def composition(self) -> GraphComposition:
        return self._graph.composition

    @property
    def name(self) -> str:
        return self._graph.name


class LoopBasedCompositeUGraph(CompositeUGraph[LoopedGraph]):
    def __init__(self, graph: LoopedGraph, iteration: Iteration):
        super().__init__(graph, iteration)
        self.graph = graph

    @property
    def subgraphs(self) -> Sequence[UGraph]:
        return _LoopUnrollingSequence(self.graph.subgraph, self.iteration, range(self.graph.count), unroll)

    @property
    def composition(self) -> GraphComposition:
        return GraphComposition.SEQUENTIAL

    @property
    def name(self) -> str:
        return self.graph.name
