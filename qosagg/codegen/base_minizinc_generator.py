import re
import textwrap
from typing import TextIO, Iterable, Union, Optional, NamedTuple

from qosagg.codegen.naming_strategy import NamingStrategy


class MiniZincGeneratorConfig:
    def __init__(self, output: bool = True, checkpoints: bool = False, reachability: bool = False,
                 taskinst_mapping: bool = False, categories: bool = False):
        self.output = output
        self.checkpoints = checkpoints
        self.reachability = reachability
        self.taskinst_mapping = taskinst_mapping
        self.categories = categories


class MznParam(NamedTuple):
    name: str
    param_type: str


class BaseMiniZincGenerator:
    def __init__(self, file: TextIO, config: MiniZincGeneratorConfig, naming: NamingStrategy):
        self.file = file
        self.naming = naming
        self.config = config

    def generate_enum(self, name: str, values: Iterable[str]):
        joined_values = ", ".join(values)
        self.file.write(f"enum {name} = {{ {joined_values} }};\n")

    def generate_array(self, index: str, name: str, values: Optional[Union[Iterable, str]] = None,
                       array_type: str = "float", variable: bool = False) -> None:
        array_type = self.conditionally_make_variable(array_type, variable)
        if values is None:
            assignment = ""
        elif isinstance(values, str):
            assignment = " = " + values
        else:
            joined_values = ', '.join([str(v) for v in values])
            assignment = f" = [{joined_values}]"
        self.file.write(f"array[{index}] of {array_type}: {name}{assignment};\n")

    def generate_constraint(self, constraint: str):
        self.file.write(f"constraint {constraint};\n")

    def generate_var(self, name: str, value: str, var_type: str, variable: bool = True):
        type_decl = self.conditionally_make_variable(var_type, variable)
        self.file.write(f"{type_decl}: {name} = {value};\n")

    def generate_set(self, name: str, value: Union[str, Iterable], set_type: str,
                     var_set: bool = False):
        if isinstance(value, str):
            value_text = value
        else:
            value_text = "{ " + ", ".join((str(v) for v in value)) + " }"
        self.file.write(f"{'var ' if var_set else ''}set of {set_type}: {name} = {value_text};\n")

    def generate_predicate(self, name: str, params: Iterable[MznParam], body: str):
        self._generate_callable("predicate", name, params, body)

    def generate_function(self, name: str, result_type: str, params: Iterable[MznParam], body: str):
        self._generate_callable(f"function {result_type}:", name, params, body)

    def _generate_callable(self, prefix: str, name: str, params: Iterable[MznParam], body: str):
        body = textwrap.indent(body, "\t")
        joined_params = ", ".join(f"{param.param_type}: {param.name}" for param in params)
        self.file.write(f"{prefix} {name}({joined_params}) =\n{body};\n")

    @staticmethod
    def is_variable_type(mzn_type: str) -> bool:
        # we consider any type as variable that contains "var" (except as part of another word)
        return re.search(r"\bvar\b", mzn_type) is not None

    @staticmethod
    def conditionally_make_variable(mzn_type: str, make_variable: bool = True) -> str:
        return mzn_type if not make_variable or BaseMiniZincGenerator.is_variable_type(mzn_type) \
            else "var " + mzn_type
