import pytest

from qosagg.domain.graph import GraphComposition, CompositeGraph, LoopedGraph
from qosagg.domain.unrolling.unrolled_graph import SingletonUGraph, unroll, CompositeUGraph
from tests.helpers import ssg, sti


def test_unroll_singleton_graph() -> None:
    # given a simple graph: Task
    graph = ssg("Task")

    # when unrolling it
    ugraph = unroll(graph)

    # then the ugraph should contain a single task instance of Task
    assert isinstance(ugraph, SingletonUGraph)
    assert ugraph._graph == graph
    assert ugraph.iteration == ()
    assert ugraph.task_instance == sti("Task")


@pytest.mark.parametrize("composition",
                         [GraphComposition.CHOICE, GraphComposition.PARALLEL, GraphComposition.SEQUENTIAL])
def test_unroll_composite_graph(composition: GraphComposition) -> None:
    # given a simple composite graph: A COMP B COMP C (with COMP in {→, ||, +})
    graph = CompositeGraph([ssg("A"), ssg("B"), ssg("C")],
                           composition, "comp")

    # when unrolling it
    ugraph = unroll(graph)

    # then there should be the right subgraphs
    assert isinstance(ugraph, CompositeUGraph)
    assert ugraph.name == "comp"
    assert ugraph.iteration == ()
    assert ugraph.composition == composition
    subgraphs = ugraph.subgraphs
    assert len(subgraphs) == 3
    assert len(subgraphs[:-1]) == 2
    with pytest.raises(TypeError):
        _ = subgraphs["first"]  # type: ignore
    task_instances = [sti("A"), sti("B"), sti("C")]
    for subgraph, task_instance in zip(subgraphs, task_instances):
        assert isinstance(subgraph, SingletonUGraph)
        assert subgraph.task_instance == task_instance


def test_unroll_looped_graph() -> None:
    # given a simple looped graph: A^3
    graph = LoopedGraph(ssg("A"), 3, "l")

    # when unrolling it
    ugraph = unroll(graph)

    # then there should be the right task instances
    assert isinstance(ugraph, CompositeUGraph)
    assert ugraph.name == "l"
    assert ugraph.iteration == ()
    assert ugraph.composition == GraphComposition.SEQUENTIAL

    subgraphs = ugraph.subgraphs
    with pytest.raises(TypeError):
        _ = subgraphs["first"]  # type: ignore
    for i, subgraph in enumerate(subgraphs):
        assert isinstance(subgraph, SingletonUGraph)
        assert subgraph.task_instance == sti("A", (i,))


def test_unroll_nested_looped_graph() -> None:
    # given a simple looped graph: A^3^2^1
    graph = LoopedGraph(LoopedGraph(LoopedGraph(ssg("A"), 3, "l3"), 2, "l2"), 1, "l1")

    # when unrolling it
    ugraph = unroll(graph)

    # then there should be the right task instances
    assert isinstance(ugraph, CompositeUGraph)
    assert ugraph.name == "l1"
    assert ugraph.iteration == ()
    assert ugraph.composition == GraphComposition.SEQUENTIAL

    subgraphs = ugraph.subgraphs
    assert len(subgraphs) == 1
    subgraph = subgraphs[0]
    assert subgraph.iteration == (0,)
    assert isinstance(subgraph, CompositeUGraph)
    assert subgraph.name == "l2"
    subsubgraphs = subgraph.subgraphs
    assert len(subsubgraphs) == 2
    for i, subsubgraph in enumerate(subsubgraphs):
        assert isinstance(subsubgraph, CompositeUGraph)
        subsubsubgraphs = subsubgraph.subgraphs
        assert len(subsubsubgraphs) == 3
        assert len(subsubsubgraphs[1:]) == 2
        for j, subsubsubgraph in enumerate(subsubsubgraphs):
            assert isinstance(subsubsubgraph, SingletonUGraph)
            assert subsubsubgraph.task_instance == sti("A", (0, i, j))
