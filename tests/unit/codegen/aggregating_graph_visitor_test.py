import pytest
from pytest_mock import MockerFixture

from qosagg.codegen.aggregating_graph_visitor import AggregatingGraphVisitor
from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.graph import CompositeGraph, GraphComposition, LoopedGraph
from qosagg.domain.unrolling.unrolled_graph import unroll
from tests.helpers import ssg


@pytest.fixture()
def naming(mocker: MockerFixture) -> NamingStrategy:
    naming = mocker.MagicMock()
    naming.name_task.side_effect = lambda task: task
    naming.name_selection_attribute.side_effect = lambda ti, attr: \
        ti.task_occ.task + "_" + attr + "".join([f"_{i + 1}" for i in ti.iteration])
    naming.name_composite.side_effect = lambda composite: composite.name
    return naming


@pytest.fixture()
def visitor(naming) -> AggregatingGraphVisitor:
    return AggregatingGraphVisitor(Aggregation("attr", "seqagg", "paragg"), naming)


def test_visit_task_instance(visitor: AggregatingGraphVisitor) -> None:
    # given a singleton task
    graph = ssg("Task")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the aggregated value should be the the value for the task
    assert actual == "Task_attr"


def test_visit_choice(visitor: AggregatingGraphVisitor) -> None:
    # given a choice
    graph = CompositeGraph([ssg("A"), ssg("B")],
                           GraphComposition.CHOICE, "c1")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the aggregated value should depend on the choice
    assert actual == "[A_attr, B_attr][c1]"


def test_visit_sequence(visitor: AggregatingGraphVisitor) -> None:
    # given a choice
    graph = CompositeGraph([ssg("A"), ssg("B")],
                           GraphComposition.SEQUENTIAL, "s1")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the aggregated value should depend on the choice
    assert actual == "seqagg([A_attr, B_attr])"


def test_visit_parallel(visitor: AggregatingGraphVisitor) -> None:
    # given a choice
    graph = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.PARALLEL, "s1")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the aggregated value should depend on the choice
    assert actual == "paragg([A_attr, B_attr])"


def test_visit_looped_graph(visitor: AggregatingGraphVisitor) -> None:
    # given a loop
    graph = LoopedGraph(ssg("A"), 3, "l1")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the aggregated value should consider all three iterations
    assert actual == "seqagg([A_attr_1, A_attr_2, A_attr_3])"


def test_visit_choice_of_optionals(visitor: AggregatingGraphVisitor) -> None:
    # given the graph (A? + B?)
    graph = CompositeGraph([
        CompositeGraph([
            ssg("A"),
            CompositeGraph([], GraphComposition.SEQUENTIAL, "nullA")
        ], GraphComposition.CHOICE, "optionalA"),
        CompositeGraph([
            ssg("B"),
            CompositeGraph([], GraphComposition.SEQUENTIAL, "nullB")
        ], GraphComposition.CHOICE, "optionalB")
    ], GraphComposition.CHOICE, "choice")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the aggregated value should depend on the choices
    assert actual == "[[A_attr, seqagg([])][optionalA], [B_attr, seqagg([])][optionalB]][choice]"
