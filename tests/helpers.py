from typing import Optional

from qosagg.domain.graph import SingletonGraph, TaskOccurrence, Task
from qosagg.domain.unrolling.task_instance import Iteration, TaskInstance


def ssg(task_name: str, occurrence: Optional[int] = None) -> SingletonGraph:
    """Returns a Simple Singleton Graph containing a task occurrence with given occurrence and a task with given name"""
    return SingletonGraph(TaskOccurrence(Task(task_name), occurrence))


def sti(task_name: str, iteration: Iteration = (), occurrence: Optional[int] = None) -> TaskInstance:
    """Returns a Simple Task Instance containing a task occurrence with given occurrence and a task with given name"""
    return TaskInstance(TaskOccurrence(Task(task_name), occurrence), iteration)
