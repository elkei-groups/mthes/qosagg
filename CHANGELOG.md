## v?.?.?

### Features

- [#26](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/26) Show version info

### Enhancements

- [#25](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/25) Speed up parsing when using primitive attribute values
  only
- Update to ANTLR version 4.10

## v1.2.0

### Features

- [#12](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/12) (
  w. [#17](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/17)) Optionally set default values for attributes
- [#5](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/5) (
  w. [#14](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/14),
  [#17](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/17),
  [#22](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/22)) Support more attribute types than floats only
- [#16](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/16) Add `null` syntax for empty graphs as well as `?`
  operator for optional (sub)graphs
- [#18](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/18) Generate detection of category changes, e.g. for
  preventing provider hopping
- [#19](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/19) Add `minizinc { … }` blocks for raw minizinc code
  between workflows

### Enhancements

- [#6](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/6) Generate `task_of_inst` array mapping task instances to
  tasks
- [#9](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/9) Generate arrays containing the choice instances for a
  choice
- [#15](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/15) Generate arrays mapping task and iteration to a task
  instance
- [#11](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/11) Show proper error message for missing attribute values
- [#23](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/23) Support multiple usage of the same task
- Reachability is now based on task instances and can be enabled with checkpoints disabled
- As often there only is one workflow, that workflow is now called `wf` instead of `wf1` by default
- User interrupts are now handled gracefully

### Fixes

- [#8](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/8) Zero-iterated loop causes broken output
- [#24](https://gitlab.com/elkei-groups/mthes/qosagg/-/issues/24) Aggregation with optional tasks broken

## v1.1.0

### Enhancements

- Create distinct service selection decision variables per loop iteration
- Make semicolon after last workflow optional

## v1.0.2

### Fixes

- Selection graph output shows wrong provision names if provisions are not sorted alphabetically

## v1.0.1

### Fixes

- Error output is printed to `stdout` instead of `stderr`

## v1.0.0

Initial release
