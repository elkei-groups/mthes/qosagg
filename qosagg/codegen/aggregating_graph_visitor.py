from typing import Dict

from qosagg.codegen.naming_strategy import NamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.graph import GraphComposition
from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph


class AggregatingGraphVisitor(UGraphVisitor[str]):
    def __init__(self, aggregation: Aggregation, naming: NamingStrategy):
        super().__init__()
        self.aggregation = aggregation
        self.aggregation_functions: Dict[GraphComposition, str] = {
            GraphComposition.PARALLEL: aggregation.parallel_aggregator,
            GraphComposition.SEQUENTIAL: aggregation.sequential_aggregator,
            # GraphComposition.CHOICE is handled separately
        }
        self.naming = naming

    def visit_task_instance(self, task_inst: TaskInstance) -> str:
        return self.naming.name_selection_attribute(task_inst, self.aggregation.attribute_name)

    def visit_composite_ugraph(self, composite: CompositeUGraph) -> str:
        subvalues = [self.visit(g) for g in composite.subgraphs]
        subvalues_array = "[" + ", ".join(subvalues) + "]"
        if composite.composition == GraphComposition.CHOICE:
            return f"{subvalues_array}[{self.naming.name_composite(composite)}]"
        else:
            function = self.aggregation_functions[composite.composition]
            return f"{function}({subvalues_array})"
