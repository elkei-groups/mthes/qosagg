from __future__ import annotations

from itertools import chain
from typing import List, Set, TypeVar, Generic, Optional, Dict, Collection

from qosagg.domain.aggregation import Aggregation
from qosagg.domain.attribute import Attribute
from qosagg.domain.category import Category
from qosagg.domain.graph import Graph, Task
from qosagg.domain.provision import Provision
from qosagg.domain.unrolling.unrolled_graph import UGraph, unroll

_GraphType = TypeVar('_GraphType', Graph, UGraph)


class Workflow(Generic[_GraphType]):
    def __init__(self, graph: _GraphType, provisions: List[Provision], aggregations: List[Aggregation],
                 categories: Collection[Category], attributes: Collection[Attribute]):
        self.graph: _GraphType = graph
        self.provisions = provisions
        self.aggregations = aggregations
        self.categories = categories
        self.attributes_by_name: Dict[str, Attribute] = {a.name: a for a in attributes}

        # add implicitly defined attributes from provisions and aggregations
        for attribute_name in chain((agg.attribute_name for agg in self.aggregations),
                                    (attr_val.name for p in provisions for attr_val in p.attributes),
                                    (cat.attribute_name for cat in categories)):
            if attribute_name not in self.attributes_by_name:
                self.attributes_by_name[attribute_name] = Attribute(attribute_name)

    def get_provision_names(self) -> List[str]:
        return [p.name for p in self.provisions]

    @property
    def attributes(self) -> Collection[Attribute]:
        return self.attributes_by_name.values()

    def get_tasks(self) -> Set[Task]:
        return self.graph.get_tasks()

    def get_default_value(self, attribute_name: str) -> Optional[str]:
        try:
            return self.attributes_by_name[attribute_name].default
        except KeyError:
            return None


class NamedWorkflow(Workflow[_GraphType]):
    def __init__(self, name: str, graph: _GraphType, provisions: List[Provision], aggregations: List[Aggregation],
                 categories: Collection[Category], attributes: Collection[Attribute]):
        super(NamedWorkflow, self).__init__(graph, provisions, aggregations, categories, attributes)
        self.name = name

    @classmethod
    def from_workflow(cls, name: str, wf: Workflow[_GraphType]):
        return cls(name, wf.graph, wf.provisions, wf.aggregations, wf.categories, wf.attributes)

    def unrolled(self) -> "NamedWorkflow"[UGraph]:
        if isinstance(self.graph, UGraph):
            return self  # type: ignore # must be NamedWorkflow[UGraph] already
        return NamedWorkflow(self.name, unroll(self.graph), self.provisions, self.aggregations, self.categories,
                             self.attributes)
