import textwrap
from io import StringIO
from typing import Callable

import pytest

from qosagg.codegen.base_minizinc_generator import MiniZincGeneratorConfig
from qosagg.codegen.graph_collector import GraphCollector
from qosagg.codegen.naming_strategy import PrefixNamingStrategy
from qosagg.codegen.reachability.reachability_generator import ReachabilityGenerator
from qosagg.domain.graph import CompositeGraph, GraphComposition, Graph, LoopedGraph
from qosagg.domain.unrolling.unrolled_graph import unroll
from tests.helpers import ssg


@pytest.fixture()
def config() -> MiniZincGeneratorConfig:
    return MiniZincGeneratorConfig(output=False, checkpoints=True, reachability=True)


@pytest.fixture()
def generate(output: StringIO, config: MiniZincGeneratorConfig) -> Callable[[Graph], None]:
    def get_generator(g: Graph):
        ugraph = unroll(g)
        return ReachabilityGenerator(ugraph, output, config, PrefixNamingStrategy("wf"), GraphCollector(ugraph))

    return lambda g: get_generator(g).generate_reachability()


def test_reachability_generation_nested_choice(generate: Callable[[Graph], None], output: StringIO) -> None:
    # given a mixed workflow (A + B) C + D
    choice2 = CompositeGraph([
        ssg("A"),
        ssg("B")
    ], GraphComposition.CHOICE, "choice2")
    choice1 = CompositeGraph([
        CompositeGraph([
            choice2,
            ssg("C")
        ], GraphComposition.SEQUENTIAL, "s1"),
        ssg("D")
    ], GraphComposition.CHOICE, "choice1")

    # when generating the MiniZinc code
    generate(choice1)

    # then the output should contain the reachabilities assignment
    text = output.getvalue()
    print(text)
    reachable_array_exp = "array[WF_TASK_INST] of var bool: wf_reachable = [" \
                          "wf_choice1 == 1 ∧ wf_choice2 == 1, wf_choice1 == 1 ∧ wf_choice2 == 2, " \
                          "wf_choice1 == 1, wf_choice1 == 2];\n"
    tasks_fun_exp = textwrap.dedent("""
        function var set of WF_TASK: wf_reachable_tasks(set of WF_TASK: ts) =
        \t{ t | t in ts where exists(ti in WF_TASK_INST)(wf_task_of_inst[ti] == t ∧ wf_reachable[ti]) };
        """)
    task_insts_fun_exp = textwrap.dedent("""
        function var set of WF_TASK_INST: wf_reachable_task_insts(set of WF_TASK_INST: tis) =
        \t{ ti | ti in tis where wf_reachable[ti] };
        """)
    checkpoints_fun_exp = textwrap.dedent("""
        function var set of WF_CHECKPOINT: wf_reachable_checkpoints(set of WF_CHECKPOINT: cps) =
        \t{ cp | cp in cps where wf_reachable[wf_task_inst_of_checkpoint[cp]] };""")
    assert reachable_array_exp in text
    assert tasks_fun_exp in text
    assert task_insts_fun_exp in text
    assert checkpoints_fun_exp in text


def test_reachability_generation_no_choice(generate: Callable[[Graph], None], output: StringIO) -> None:
    # given a simple workflow A -> B
    graph = CompositeGraph([
        ssg("A"),
        ssg("B")
    ], GraphComposition.SEQUENTIAL, "s1")

    # when generating the MiniZinc code
    generate(graph)

    # then the output should contain the reachabilities assignment
    text = output.getvalue()
    print(text)
    reachable_array_exp = "array[WF_TASK_INST] of var bool: wf_reachable = [true, true];\n"
    assert reachable_array_exp in text


def test_reachability_looped_choice(generate: Callable[[Graph], None], output: StringIO) -> None:
    # given a looped choice (A + B)^3
    choice1 = CompositeGraph([
        ssg("A"),
        ssg("B")
    ], GraphComposition.CHOICE, "c")
    graph = LoopedGraph(choice1, 3, "loop1")

    # when generating the MiniZinc code
    generate(graph)

    # then the output should contain the reachabilities assignment
    text = output.getvalue()
    print(text)
    reachable_array_exp = "array[WF_TASK_INST] of var bool: wf_reachable = " \
                          "[wf_c_1 == 1, wf_c_2 == 1, wf_c_3 == 1, wf_c_1 == 2, wf_c_2 == 2, wf_c_3 == 2];\n"
    assert reachable_array_exp in text
