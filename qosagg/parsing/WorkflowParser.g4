parser grammar WorkflowParser;
options { tokenVocab=WorkflowLexer; }

parseTLDs : tlds EOF;
parseWorkflow : workflow EOF;

tlds: (topLevelDeclaration SEMICOLON?)*;
topLevelDeclaration: workflowDecl | minizincDecl;
minizincDecl: MINIZINC bodyStart=LBRACE .*? bodyStop=RBRACE;
workflowDecl: WORKFLOW name=ID? COLON? LBRACE workflow RBRACE;
workflow: (provision | aggregation | attributeDecl | category)* graphDecl
          (provision | aggregation | attributeDecl | category)*;
graphDecl: GRAPH COLON graph SEMICOLON;
graph: graph op=EXP iterations=INT
    | graph op=QUESTIONMARK
    | graph op=RARROW? graph
    | graph op=CHOICE graph
    | graph op=PARALLEL graph
    | LPARENTHESES graph RPARENTHESES
    | ID
    | op=NULL;
provision: PROVISION ID FOR ids (COLON attributeEqs)? SEMICOLON;
ids: ID (COMMA ID)*;
attributeEqs: attributeEq (COMMA attributeEq)*;
attributeEq: name=ID IS attributeValue;
attributeDecl: ATTRIBUTE name=ID (OF attributeType)? (DEFAULT default=attributeValue)? SEMICOLON;
aggregation: AGGREGATION name=ID COLON sequentialAgg=ID (COMMA parallelAgg=ID)? SEMICOLON;
category: CATEGORY name=ID SEMICOLON;
attributeValue: .+?;
attributeType: .+?;
