import re
from io import StringIO
from typing import Optional

import pytest
from pytest_mock import MockerFixture

from qosagg.codegen.output.output_generating_graph_visitor import OutputGeneratingGraphVisitor
from qosagg.domain.graph import SingletonGraph, Graph, LoopedGraph, CompositeGraph, GraphComposition, Task, \
    TaskOccurrence
from qosagg.domain.unrolling.task_instance import Iteration
from qosagg.domain.unrolling.unrolled_graph import unroll


@pytest.fixture()
def visitor(mocker: MockerFixture) -> OutputGeneratingGraphVisitor:
    naming = mocker.MagicMock()
    naming.name_provision_names_array.return_value = "p"
    naming.name_selection_array.return_value = "s"

    def name_with_iteration(base_name: str, occurrence: Optional[int], iteration: Iteration):
        return base_name + \
               ("" if occurrence is None else "_" + str(occurrence)) + \
               ("" if iteration is None else "".join(["_" + str(i) for i in iteration]))

    naming.name_task_instance.side_effect = lambda ti: name_with_iteration(ti.task_occ.task, ti.task_occ.occurrence,
                                                                           ti.iteration)
    naming.name_composite.side_effect = lambda c: name_with_iteration(c.name, None, c.iteration)
    return OutputGeneratingGraphVisitor(naming)


def simulate_output(g: Graph, visitor: OutputGeneratingGraphVisitor) -> str:
    parts = visitor.visit(unroll(g))
    output = "".join(parts)
    output = re.sub(r"if fix\(choice[0-9](?:_[0-9]+)*\) == ([0-9]+) then (.+?) else \"\" endif",
                    lambda m: m.group(2) if m.group(1) == "1" else "", output)  # resolve choices
    output = re.sub(r"p\[fix\(s\[([a-zA-Z_0-9]+)]\)]", lambda m: "p" + m.group(1), output)  # resolve decisions
    output = re.sub(r"\"(.*?)\"", lambda m: m.group(1), output)  # remove double quotes
    return output


def test_visit_singleton_graph(visitor: OutputGeneratingGraphVisitor):
    # given a singleton graph
    graph = SingletonGraph(TaskOccurrence(Task("Task")))

    # when generating output for it and simulating what MiniZinc does to it
    sim_output = simulate_output(graph, visitor)

    # then the output should match what we'd expect
    assert sim_output == "Task=pTask"


@pytest.mark.parametrize(["subgraph", "suboutput"], [
    (SingletonGraph(TaskOccurrence(Task("A"))), "X"),
    (CompositeGraph([SingletonGraph(TaskOccurrence(Task("A"))), SingletonGraph(TaskOccurrence(Task("B")))],
                    GraphComposition.PARALLEL, "p"), "(X | X)"),
    (CompositeGraph([SingletonGraph(TaskOccurrence(Task("A"))), SingletonGraph(TaskOccurrence(Task("B")))],
                    GraphComposition.CHOICE, "choice1"), "X"),
    (CompositeGraph([SingletonGraph(TaskOccurrence(Task("A"))), SingletonGraph(TaskOccurrence(Task("B")))],
                    GraphComposition.SEQUENTIAL, "s"), "X → X"),
    (LoopedGraph(SingletonGraph(TaskOccurrence(Task("A"))), 1, "l"), "X")
])
def test_visit_looped_graph(subgraph: Graph, suboutput: str, mocker: MockerFixture,
                            visitor: OutputGeneratingGraphVisitor):
    # given a looped graph
    graph = LoopedGraph(subgraph, 3, "l1")

    # when generating output for it and simulating what MiniZinc does to it
    mocker.patch.object(visitor, "visit_task_instance", return_value=["\"X\""])
    sim_output = simulate_output(graph, visitor)

    # then the output should match what we'd expect
    assert sim_output == f"{suboutput} → {suboutput} → {suboutput}"


@pytest.mark.parametrize(["graph", "output"], [
    (CompositeGraph([SingletonGraph(TaskOccurrence(Task("A"))), SingletonGraph(TaskOccurrence(Task("B")))],
                    GraphComposition.PARALLEL, "p"), "(X | X)"),
    (CompositeGraph([SingletonGraph(TaskOccurrence(Task("A"))), SingletonGraph(TaskOccurrence(Task("B")))],
                    GraphComposition.CHOICE, "choice1"), "X"),
    (CompositeGraph([SingletonGraph(TaskOccurrence(Task("A"))), SingletonGraph(TaskOccurrence(Task("B")))],
                    GraphComposition.SEQUENTIAL, "s"), "X → X")
])
def test_visit_composite_graph(graph, output, mocker, visitor: OutputGeneratingGraphVisitor):
    # given a composite graph
    # when generating output for it and simulating what MiniZinc does to it
    mocker.patch.object(visitor, "visit_task_instance", return_value=["\"X\""])
    sim_output = simulate_output(graph, visitor)

    # then the output should match what we'd expect
    assert sim_output == output


def test_null_in_sequence(output: StringIO, visitor: OutputGeneratingGraphVisitor) -> None:
    # given A -> null -> B
    graph = CompositeGraph([
        SingletonGraph(TaskOccurrence(Task("A"))),
        CompositeGraph([], GraphComposition.SEQUENTIAL, "null1"),
        SingletonGraph(TaskOccurrence(Task("B")))
    ], GraphComposition.SEQUENTIAL, "s1")

    # when generating the MiniZinc code
    actual = simulate_output(graph, visitor)

    # then the output should look like A -> B
    assert actual == "A=pA → B=pB"


def test_null(output: StringIO, visitor: OutputGeneratingGraphVisitor) -> None:
    # given graph: null
    graph = CompositeGraph([], GraphComposition.SEQUENTIAL, "null1")

    # when generating the MiniZinc code
    actual = simulate_output(graph, visitor)

    # then the output should look like A -> B
    assert actual == ""
