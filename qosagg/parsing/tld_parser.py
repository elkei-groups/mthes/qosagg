from io import TextIOWrapper
from typing import Union, List, Optional

from antlr4 import InputStream, CommonTokenStream

from qosagg.parsing.ThrowingErrorListener import ThrowingErrorListener, ParsingError
from qosagg.parsing.generated.FastWorkflowParser import FastWorkflowParser
from qosagg.parsing.generated.WorkflowLexer import WorkflowLexer
from qosagg.parsing.generated.WorkflowParser import WorkflowParser
from qosagg.parsing.to_domain_visitor import TopLevelDeclaration, ToDomainVisitor


def parse(infile: TextIOWrapper, non_primitive_attributes: Optional[bool] = None) -> List[TopLevelDeclaration]:
    ctx = _parse_to_ast(infile, non_primitive_attributes)
    return ToDomainVisitor().visit(ctx)


def _parse_to_ast(infile: TextIOWrapper, non_primitive_attributes: Optional[bool] = None) -> \
        Union[FastWorkflowParser.ParseTLDsContext, WorkflowParser.ParseTLDsContext]:
    use_fast_parser = non_primitive_attributes is None or not non_primitive_attributes
    use_slow_parser = non_primitive_attributes is None or non_primitive_attributes

    lexer = WorkflowLexer(InputStream(infile.read()))
    token_stream = CommonTokenStream(lexer)

    if use_fast_parser:
        fast_parser = FastWorkflowParser(token_stream)
        fast_parser.removeErrorListeners()
        fast_parser.addErrorListener(ThrowingErrorListener())
        try:
            return fast_parser.parseTLDs()
        except ParsingError as pe:
            if not use_slow_parser:
                raise pe
            else:
                token_stream.reset()

    slow_parser = WorkflowParser(token_stream)
    slow_parser.addErrorListener(ThrowingErrorListener())
    return slow_parser.parseTLDs()
