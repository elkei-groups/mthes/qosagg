from qosagg.domain.attribute import Attribute
from qosagg.domain.graph import Graph
from qosagg.domain.unrolling.unrolled_graph import UGraph, SingletonUGraph
from qosagg.domain.workflow import NamedWorkflow, Workflow
from tests.helpers import ssg


def test_unrolling_unrolled_returns_same():
    # given a named workflow with unrolled graph
    wf: NamedWorkflow[UGraph] = NamedWorkflow("wf", SingletonUGraph(ssg("A"), ()), [], [], [], [])

    # when unrolling
    unrolled = wf.unrolled()

    # then they should be the same instance
    assert unrolled is wf


def test_get_default_attribute_value() -> None:
    # given a workflow
    wf: Workflow[Graph] = Workflow(ssg("A"), [], [], [], [
        Attribute("attr_no_default"),
        Attribute("attr_with_default", default="2"),
    ])

    # then the default should be returned or none if none exists
    assert wf.get_default_value("attr_with_default") == "2"
    assert wf.get_default_value("attr_no_default") is None
    assert wf.get_default_value("no_attr") is None
