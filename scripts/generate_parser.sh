#!/bin/sh

if [ $# -eq 0 ]; then
  jar="/usr/local/lib/antlr/antlr.jar"
else
  jar="$1"
fi
antlr="java -jar $jar"

$antlr -Dlanguage=Python3 -o generated WorkflowLexer.g4
$antlr -Dlanguage=Python3 -o generated -visitor -listener WorkflowParser.g4

cp WorkflowParser.g4 FastWorkflowParser.g4
sed -i 's/WorkflowParser/FastWorkflowParser/g; s/attributeValue: .+?;/attributeValue: FLOAT | INT | ID;/g; s/attributeType: .+?;/attributeType: ID;/g' FastWorkflowParser.g4
$antlr -Dlanguage=Python3 -o generated -visitor -listener FastWorkflowParser.g4