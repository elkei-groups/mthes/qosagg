from typing import Callable

from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph


class TaskInstanceVisitor(UGraphVisitor[None]):
    def __init__(self, callback: Callable[[TaskInstance], None]) -> None:
        self._callback = callback

    def visit_task_instance(self, task_inst: TaskInstance) -> None:
        self._callback(task_inst)

    def visit_composite_ugraph(self, ugraph: CompositeUGraph) -> None:
        for subgraph in ugraph.subgraphs:
            self.visit(subgraph)
