class Aggregation:
    def __init__(self, attribute_name: str, sequential_aggregator: str, parallel_aggregator: str) -> None:
        self.attribute_name = attribute_name
        self.sequential_aggregator = sequential_aggregator
        self.parallel_aggregator = parallel_aggregator

    def __eq__(self, o: object) -> bool:
        return isinstance(o, Aggregation) and \
               self.attribute_name == o.attribute_name and \
               self.sequential_aggregator == o.sequential_aggregator and \
               self.parallel_aggregator == o.parallel_aggregator

    def __str__(self) -> str:
        return f"aggregation {self.attribute_name}: " \
               f"{self.sequential_aggregator}, {self.parallel_aggregator}"
