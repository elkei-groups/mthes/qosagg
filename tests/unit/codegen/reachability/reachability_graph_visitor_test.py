import pytest
from pytest_mock import MockerFixture

from qosagg.codegen.reachability.reachability_graph_visitor import ReachabilityGraphVisitor
from qosagg.domain.graph import LoopedGraph, CompositeGraph, GraphComposition
from qosagg.domain.unrolling.unrolled_graph import unroll, CompositeUGraph
from tests.helpers import ssg, sti


@pytest.fixture()
def visitor(mocker: MockerFixture) -> ReachabilityGraphVisitor:
    def name(composite: CompositeUGraph):
        result = composite.name
        if composite.iteration:
            result += "_" + "_".join(str(i + 1) for i in composite.iteration)
        return result

    naming = mocker.MagicMock()
    naming.name_composite.side_effect = name
    return ReachabilityGraphVisitor(naming)


def test_visit_zero_loop(visitor: ReachabilityGraphVisitor):
    # given a looped graph with 0 iterations: A^0
    graph = LoopedGraph(ssg("A"), 0, "l1")

    # when collecting reachabilities
    visitor.visit(unroll(graph))

    # then there should be no task instance
    assert not visitor.reachabilities


def test_visit_graph_without_choice(visitor: ReachabilityGraphVisitor):
    # given a graph with no choices: (A | B)^2 -> C
    graph = CompositeGraph(
        [LoopedGraph(
            CompositeGraph([ssg("A"), ssg("B")], GraphComposition.PARALLEL, "p1"),
            2, "l1"),
            ssg("C")], GraphComposition.SEQUENTIAL, "s1")

    # when collecting reachabilities
    visitor.visit(unroll(graph))

    # then all tasks should be reachable
    for i in (0, 1):
        assert visitor.reachabilities[sti("A", (i,))] == "true"
        assert visitor.reachabilities[sti("B", (i,))] == "true"
        assert visitor.reachabilities[sti("C", (i,))] == "false"
        assert visitor.reachabilities[sti("NonExistent", (i,))] == "false"
    assert visitor.reachabilities[sti("C")] == "true"
    assert visitor.reachabilities[sti("A")] == "false"
    assert visitor.reachabilities[sti("A", (3,))] == "false"


def test_visit_nested_choice(visitor: ReachabilityGraphVisitor):
    # given a nested choice: ((A + B) + C) -> D
    choice2 = CompositeGraph([
        ssg("A"),
        ssg("B")
    ], GraphComposition.CHOICE, "choice2")
    choice1 = CompositeGraph([
        choice2,
        ssg("C")
    ], GraphComposition.CHOICE, "choice1")
    graph = CompositeGraph([
        choice1,
        ssg("D")
    ], GraphComposition.SEQUENTIAL, "s1")

    # when collecting reachabilities
    visitor.visit(unroll(graph))

    # then D should always be reachable and A, B, C conditionally
    r = visitor.reachabilities
    assert r[sti("D", ())] == "true"
    assert r[sti("C", ())] == "choice1 == 2"
    assert r[sti("B")] == "choice1 == 1 ∧ choice2 == 2"
    assert r[sti("A", ())] == "choice1 == 1 ∧ choice2 == 1"


def test_visit_looped_choice(visitor: ReachabilityGraphVisitor):
    # given a choice in a loop: (A+B)^2
    choice1 = CompositeGraph([
        ssg("A"),
        ssg("B")
    ], GraphComposition.CHOICE, "choice1")
    graph = LoopedGraph(choice1, 2, "l1")

    # when collecting reachabilities
    visitor.visit(unroll(graph))

    # then the reachabilities should match our expectation
    r = visitor.reachabilities
    assert r[sti("A", (0,))] == "choice1_1 == 1"
    assert r[sti("B", (0,))] == "choice1_1 == 2"
    assert r[sti("A", (1,))] == "choice1_2 == 1"
    assert r[sti("B", (1,))] == "choice1_2 == 2"
