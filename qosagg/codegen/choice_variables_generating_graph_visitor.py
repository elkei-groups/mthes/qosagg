from collections import defaultdict
from typing import Callable, TextIO, Dict, List, NamedTuple

from qosagg.domain.graph import GraphComposition
from qosagg.domain.graph_visitor import UGraphVisitor
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph


class ChoiceAggregation(NamedTuple):
    variables: List[str] = []
    max_options: int = 0


class ChoiceVariablesGeneratingGraphVisitor(UGraphVisitor[None]):
    def __init__(self, file: TextIO, get_choice_variable_name: Callable[[CompositeUGraph], str]):
        super().__init__()
        self.file = file
        self._get_choice_variable_name = get_choice_variable_name
        self.variables_by_choice: Dict[str, ChoiceAggregation] = defaultdict(lambda: ChoiceAggregation())

    def visit_task_instance(self, task_inst: TaskInstance) -> None:
        return

    def visit_composite_ugraph(self, composite: CompositeUGraph) -> None:
        if composite.composition == GraphComposition.CHOICE:
            num_choices = len(composite.subgraphs)
            name = self._get_choice_variable_name(composite)
            aggregation = self.variables_by_choice[composite.name]
            self.variables_by_choice[composite.name] = ChoiceAggregation(
                aggregation.variables + [name],
                max(aggregation.max_options, num_choices)
            )
            self.file.write(f"var 1..{num_choices}: {name};\n")
        for child in composite.subgraphs:
            self.visit(child)
