from typing import Dict, Union

from qosagg.domain.graph import TaskOccurrence, Task


class TaskOccurrencesTracer:
    def __init__(self) -> None:
        self.task_occurrences: Dict[Task, Union[TaskOccurrence, int]] = dict()

    def get_next_task_occurrence(self, task: Task) -> TaskOccurrence:
        try:
            existing = self.task_occurrences[task]
            if isinstance(existing, TaskOccurrence):
                # this is the second occurrence
                return self._add_second_occurrence(task, existing)
            else:
                # this is the 3rd/4th/… occurrence
                return self._add_subsequent_occurrence(task, existing)
        except KeyError:
            # this is the first occurrence
            return self._add_first_occurrence(task)

    def _add_first_occurrence(self, task: Task) -> TaskOccurrence:
        occurrence = TaskOccurrence(task)
        self.task_occurrences[task] = occurrence
        return occurrence

    def _add_second_occurrence(self, task: Task, first: TaskOccurrence) -> TaskOccurrence:
        first.is_sole_task_occurrence = False
        self.task_occurrences[task] = 2
        return TaskOccurrence(task, 1)

    def _add_subsequent_occurrence(self, task: Task, previous_occurrences: int) -> TaskOccurrence:
        self.task_occurrences[task] = previous_occurrences + 1
        return TaskOccurrence(task, previous_occurrences)
