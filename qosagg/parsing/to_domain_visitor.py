import re
import textwrap
from typing import List, Set, Optional, Union, NewType

from antlr4 import ParserRuleContext

from qosagg.domain.aggregation import Aggregation
from qosagg.domain.attribute import Attribute
from qosagg.domain.category import Category
from qosagg.domain.graph import Graph, SingletonGraph, GraphComposition, CompositeGraph, LoopedGraph, Task
from qosagg.domain.provision import AttributeValue, Provision
from qosagg.domain.workflow import Workflow, NamedWorkflow
from qosagg.parsing.generated.FastWorkflowParserVisitor import FastWorkflowParserVisitor
from qosagg.parsing.generated.WorkflowParser import WorkflowParser
from qosagg.parsing.generated.WorkflowParserVisitor import WorkflowParserVisitor
from qosagg.parsing.task_occurrences_tracer import TaskOccurrencesTracer

MiniZincBlock = NewType("MiniZincBlock", str)


TopLevelDeclaration = Union[NamedWorkflow[Graph], MiniZincBlock]


class ToDomainVisitor(WorkflowParserVisitor, FastWorkflowParserVisitor):
    """ANTLR parse tree visitor converting the ANTLR workflows parse tree to a list of workflows as domain objects"""

    def __init__(self) -> None:
        super().__init__()
        self.used_names: Set[str] = set()
        self.task_occurrences_tracer = TaskOccurrencesTracer()

    def visitParseTLDs(self, ctx: WorkflowParser.ParseTLDsContext) -> List[TopLevelDeclaration]:
        return self.visit(ctx.tlds())

    def visitParseWorkflow(self, ctx: WorkflowParser.ParseWorkflowContext) -> NamedWorkflow[Graph]:
        return self.visit(ctx.workflow())

    def visitTlds(self, ctx: WorkflowParser.TldsContext) -> List[TopLevelDeclaration]:
        return [self.visit(tld) for tld in ctx.topLevelDeclaration()]

    def visitTopLevelDeclaration(self, ctx: WorkflowParser.TopLevelDeclarationContext) -> TopLevelDeclaration:
        if ctx.workflowDecl() is not None:
            return self.visit(ctx.workflowDecl())
        else:
            return self.visit(ctx.minizincDecl())

    def visitWorkflowDecl(self, ctx: WorkflowParser.WorkflowDeclContext) -> NamedWorkflow[Graph]:
        workflow: Workflow[Graph] = self.visit(ctx.workflow())
        name: str = self._reserve_next_available_name(
            "wf" if ctx.name is None else ctx.name.text, False)  # type: ignore
        return NamedWorkflow.from_workflow(name, workflow)

    def _reserve_next_available_name(self, prefix: str, enforce_number: bool = True) -> str:
        name = prefix if not enforce_number else prefix + "1"
        counter = 2
        while name in self.used_names:
            name = prefix + str(counter)
            counter += 1
        self.used_names.add(name)
        return name

    def visitWorkflow(self, ctx: WorkflowParser.WorkflowContext) -> Workflow:
        graph: Graph = self.visit(ctx.graphDecl())
        provisions: List[Provision] = [self.visit(prov_ctx) for prov_ctx in ctx.provision()]
        aggregations: List[Aggregation] = [self.visit(agg_ctx) for agg_ctx in ctx.aggregation()]
        attributes: List[Attribute] = [self.visit(attr_ctx) for attr_ctx in ctx.attributeDecl()]
        categories: List[Category] = [self.visit(cat_ctx) for cat_ctx in ctx.category()]
        return Workflow(graph, provisions, aggregations, categories, attributes)  # name will be set by declaration

    def visitGraphDecl(self, ctx: WorkflowParser.GraphDeclContext) -> Graph:
        return self.visit(ctx.graph())

    def visitGraph(self, ctx: WorkflowParser.GraphContext) -> Graph:
        subgraphs: List[Graph] = [self.visit(subgraph_ctx) for subgraph_ctx in ctx.graph()]
        if len(subgraphs) == 0:
            if ctx.op is not None and ctx.op.type == WorkflowParser.NULL:
                # NULL graph
                return CompositeGraph([], GraphComposition.SEQUENTIAL, self._reserve_next_available_name("null", True))
            else:
                # ID only
                assert ctx.ID() is not None
                task = Task(ctx.ID().getText())
                task_occurrence = self.task_occurrences_tracer.get_next_task_occurrence(task)
                return SingletonGraph(task_occurrence)
        elif len(subgraphs) == 1:
            # parentheses, looped or optional rule
            subgraph = subgraphs[0]
            if ctx.op is not None and ctx.op.type == WorkflowParser.EXP:
                iterations = ctx.iterations
                assert iterations is not None and iterations.text is not None
                name = self._reserve_next_available_name("loop", enforce_number=True)
                return LoopedGraph(subgraph, int(iterations.text), name)
            if ctx.op is not None and ctx.op.type == WorkflowParser.QUESTIONMARK:
                return CompositeGraph([subgraph, CompositeGraph([], GraphComposition.SEQUENTIAL,
                                                                self._reserve_next_available_name("null"))],
                                      GraphComposition.CHOICE,
                                      self._reserve_next_available_name("optional", True))
            else:
                return subgraph
        else:
            # composite graph
            operator = {
                WorkflowParser.PARALLEL: GraphComposition.PARALLEL,
                WorkflowParser.RARROW: GraphComposition.SEQUENTIAL,
                WorkflowParser.CHOICE: GraphComposition.CHOICE
            }[ctx.op.type if ctx.op is not None else WorkflowParser.RARROW]  # type: ignore
            assert operator is not None
            name_prefix = {
                GraphComposition.PARALLEL: "parallel",
                GraphComposition.SEQUENTIAL: "sequence",
                GraphComposition.CHOICE: "choice"
            }[operator]
            name = self._reserve_next_available_name(name_prefix, enforce_number=True)
            return CompositeGraph(subgraphs, operator, name)

    def visitProvision(self, ctx: WorkflowParser.ProvisionContext) -> Provision:
        name: str = ctx.ID().getText()
        tasks: List[Task] = [Task(task_id) for task_id in self.visit(ctx.ids())]
        attributes: List[AttributeValue] = self.visit(ctx.attributeEqs()) if ctx.attributeEqs() is not None else []
        return Provision(name, set(tasks), set(attributes))

    def visitIds(self, ctx: WorkflowParser.IdsContext) -> List[str]:
        return [id_ctx.getText() for id_ctx in ctx.ID()]

    def visitAttributeEqs(self, ctx: WorkflowParser.AttributeEqsContext) -> List[AttributeValue]:
        return [self.visit(attribute_eq_ctx) for attribute_eq_ctx in ctx.attributeEq()]

    def visitAttributeEq(self, ctx: WorkflowParser.AttributeEqContext) -> AttributeValue:
        name: str = ctx.ID().getText()
        value: str = self.visit(ctx.attributeValue())
        return AttributeValue(name, value)

    def visitAttributeDecl(self, ctx: WorkflowParser.AttributeDeclContext):
        assert ctx.name is not None
        attribute_name: str = ctx.name.text
        var_type: str = self.visit(ctx.attributeType()) if ctx.attributeType() is not None else "float"
        default_value: Optional[str] = ctx.default.getText() if ctx.default is not None else None
        return Attribute(attribute_name, var_type, default_value)

    def visitAggregation(self, ctx: WorkflowParser.AggregationContext) -> Aggregation:
        assert ctx.name is not None
        attribute_name: str = ctx.name.text
        sequential_agg: str = ctx.sequentialAgg.text
        parallel_agg: str = ctx.parallelAgg.text if ctx.parallelAgg is not None else sequential_agg
        return Aggregation(attribute_name, sequential_agg, parallel_agg)

    def visitAttributeValue(self, ctx: WorkflowParser.AttributeValueContext) -> str:
        return self._get_text_including_hidden(ctx, ctx.parser)

    def visitAttributeType(self, ctx: WorkflowParser.AttributeTypeContext) -> str:
        return self._get_text_including_hidden(ctx, ctx.parser)

    def visitCategory(self, ctx: WorkflowParser.CategoryContext) -> Category:
        name: str = ctx.name.text  # type: ignore
        return Category(name)

    def visitMinizincDecl(self, ctx: WorkflowParser.MinizincDeclContext) -> MiniZincBlock:
        assert ctx.bodyStop is not None and ctx.bodyStart is not None
        start: int = ctx.bodyStart.tokenIndex + 1
        stop: int = ctx.bodyStop.tokenIndex - 1
        content = ctx.parser.getTokenStream().getText(start, stop)
        return MiniZincBlock(self._dedent_block(content))

    @staticmethod
    def _dedent_block(string: str):
        string = string.rstrip()  # remove trailing whitespace
        string = re.sub(r"^[\s]*\n", "", string)  # remove leading empty lines
        string = textwrap.dedent(string)  # remove shared indentation
        return string

    @staticmethod
    def _get_text_including_hidden(ctx: ParserRuleContext, parser: WorkflowParser):
        return parser.getTokenStream().getText(ctx.start, ctx.stop)
