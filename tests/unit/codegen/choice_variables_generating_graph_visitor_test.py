import textwrap
from io import StringIO

import pytest

from qosagg.codegen.choice_variables_generating_graph_visitor import ChoiceVariablesGeneratingGraphVisitor, \
    ChoiceAggregation
from qosagg.domain.graph import CompositeGraph, GraphComposition, LoopedGraph
from qosagg.domain.unrolling.unrolled_graph import CompositeUGraph, unroll
from tests.helpers import ssg


@pytest.fixture
def visitor(output: StringIO) -> ChoiceVariablesGeneratingGraphVisitor:
    def name_choice_variable(ugraph: CompositeUGraph) -> str:
        return ugraph.name + ("" if not ugraph.iteration else
                              "_" + "_".join([str(i + 1) for i in ugraph.iteration]))

    return ChoiceVariablesGeneratingGraphVisitor(output, name_choice_variable)


def test_looped_choice(output: StringIO, visitor: ChoiceVariablesGeneratingGraphVisitor):
    # given a graph (A+B)^3 -> (C+D+E)
    graph = CompositeGraph([
        LoopedGraph(CompositeGraph([ssg("A"), ssg("B")], GraphComposition.CHOICE, "c1"), 3, "l1"),
        CompositeGraph([ssg("C"), ssg("D"), ssg("E")], GraphComposition.CHOICE, "c2")
    ], GraphComposition.SEQUENTIAL, "s1")

    # when visiting it
    visitor.visit(unroll(graph))

    # then output and aggregations should math our expectations
    agg = visitor.variables_by_choice
    assert len(agg.keys()) == 2
    assert agg["c1"] == ChoiceAggregation(["c1_1", "c1_2", "c1_3"], 2)
    assert agg["c2"] == ChoiceAggregation(["c2"], 3)

    actual = output.getvalue()
    print(actual)
    assert actual == textwrap.dedent("""\
        var 1..2: c1_1;
        var 1..2: c1_2;
        var 1..2: c1_3;
        var 1..3: c2;
        """)
