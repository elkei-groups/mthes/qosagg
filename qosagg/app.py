import argparse
import importlib.metadata
import sys
from io import TextIOWrapper
from typing import List, Optional, Iterable, TextIO

from qosagg.codegen.minizinc_generator import MiniZincGenerator, MiniZincGeneratorConfig
from qosagg.domain.workflow import NamedWorkflow
from qosagg.parsing import tld_parser
from qosagg.parsing.ThrowingErrorListener import ParsingError
from qosagg.parsing.to_domain_visitor import TopLevelDeclaration
from qosagg.user_input_error import UserInputError


def run(args: Optional[List[str]] = None):
    """Transforms workflow specifications to MiniZinc code according to the CLI options in `args`"""
    try:
        parsed_args = _parse_args(args)
        config = MiniZincGeneratorConfig(parsed_args.out, parsed_args.checkpoints, parsed_args.reachability,
                                         parsed_args.taskinst_map, parsed_args.categories)
        transform(config, parsed_args.infile, parsed_args.outfile, parsed_args.non_primitive_attributes)
    except KeyboardInterrupt:
        sys.exit(3)


def _parse_args(args: Optional[List[str]] = None):
    parser = argparse.ArgumentParser(description="Parses SoC workflows and aggregates the QoS attributes")
    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    parser.add_argument('--output', '--out', dest="out", action=argparse.BooleanOptionalAction, default=True,
                        help='generate MiniZinc output statements')
    parser.add_argument('--checkpoints', action=argparse.BooleanOptionalAction, default=True,
                        help='add aggregations for before/after tasks')
    parser.add_argument('--reachability', action=argparse.BooleanOptionalAction, default=True,
                        help='provide reachability of tasks')
    parser.add_argument('--taskinst-mapping', action=argparse.BooleanOptionalAction, default=True, dest="taskinst_map",
                        help='provide mappings from iteration to task instance for each task')
    parser.add_argument('--categories', action=argparse.BooleanOptionalAction, default=True, dest="categories",
                        help='generate category change detections (requires taskinst mapping)')
    parser.add_argument('--non-primitive-attributes', action=argparse.BooleanOptionalAction, default=None,
                        dest="non_primitive_attributes",
                        help="enforce/skip usage of fast parser (By default, a faster parser for primitive attribute "
                             "values is used until parsing fails. Then, it is replaced by a slower one that allows "
                             "arbitrary values.)")
    parser.add_argument('--version', '-v', action="version", version=f'%(prog)s {get_version_safely()}')
    parsed_args = parser.parse_args(args)
    if parsed_args.categories and not parsed_args.taskinst_map:
        print("Warning: Category generation implicitly requires task instance mappings.\n", file=sys.stderr)
        parsed_args.taskinst_map = True
    return parsed_args


def get_version_safely() -> str:
    try:
        return importlib.metadata.version("qosagg")
    except Exception:
        return "unknown"


def transform(config: MiniZincGeneratorConfig, infile: TextIOWrapper, outfile: TextIOWrapper,
              non_primitive_attributes: Optional[bool] = None):
    """Generates MiniZinc code in `outfile` for the workflows in `infile`, according to the configuration `config`"""
    try:
        tlds = tld_parser.parse(infile, non_primitive_attributes)
        _show_category_warning_if_appropriate(config, tlds)
        for (index, tld) in enumerate(tlds):
            if index > 0:
                outfile.write("\n\n")
            transform_top_level_declaration(tld, outfile, config)
    except ParsingError as e:
        print(e, file=sys.stderr)
        exit(1)
    except UserInputError as e:
        print(e, file=sys.stderr)
        exit(2)


def _show_category_warning_if_appropriate(config: MiniZincGeneratorConfig, tlds: Iterable[TopLevelDeclaration]):
    if not config.categories and any(isinstance(wf, NamedWorkflow) and wf.categories for wf in tlds):
        print("Warning: Category generation is disabled, but at least one workflow declares categories.\n",
              file=sys.stderr)


def transform_top_level_declaration(tld: TopLevelDeclaration, outfile: TextIO, config: MiniZincGeneratorConfig) -> None:
    if isinstance(tld, NamedWorkflow):
        unrolled = tld.unrolled()
        MiniZincGenerator(unrolled, outfile, config).generate()
    else:
        outfile.write(tld)
    outfile.flush()


if __name__ == '__main__':
    run()
