from typing import Optional, Tuple

import pytest

from qosagg.codegen.naming_strategy import PrefixNamingStrategy, NamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.graph import Task, CompositeGraph, GraphComposition, TaskOccurrence
from qosagg.domain.provision import Provision
from qosagg.domain.unrolling.task_instance import Checkpoint, CheckpointType, Iteration
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, ClassicCompositeUGraph


@pytest.fixture()
def strategy():
    return PrefixNamingStrategy("wf1")


def test_name_provision_enum(strategy: NamingStrategy) -> None:
    assert strategy.name_provision_enum() == "WF1_PROVISION"


def test_name_task_enum(strategy: NamingStrategy) -> None:
    assert strategy.name_task_enum() == "WF1_TASK"


def test_name_selection_attribute(strategy: NamingStrategy) -> None:
    assert strategy.name_selection_attribute(TaskInstance(TaskOccurrence(Task("A"), None), (0, 1)), "cost") == \
           "wf1_A_cost_1_2"
    assert strategy.name_selection_attribute(TaskInstance(TaskOccurrence(Task("A"), 0)), "cost") == \
           "wf1_A_cost_1"


def test_name_selection_array(strategy: NamingStrategy) -> None:
    assert strategy.name_selection_array() == "wf1_selection"


def test_name_attribute_array(strategy: NamingStrategy) -> None:
    assert strategy.name_attribute_array("cost") == "wf1_cost"


def test_name_provision_names_array(strategy: NamingStrategy) -> None:
    assert strategy.name_provision_names_array() == "wf1_provision_names"


def test_name_aggregation(strategy: NamingStrategy) -> None:
    assert strategy.name_aggregation(Aggregation("cost", "sum", "sum")) == "wf1_aggregated_cost"


def test_name_choice_variable(strategy: NamingStrategy) -> None:
    assert strategy.name_composite(ClassicCompositeUGraph(CompositeGraph([], GraphComposition.CHOICE, "choice7"), ())) \
           == "wf1_choice7"
    assert strategy.name_composite(ClassicCompositeUGraph(CompositeGraph([], GraphComposition.PARALLEL, "p"),
                                                          (1, 3, 5))) == "wf1_p_2_4_6"


def test_name_task(strategy: NamingStrategy) -> None:
    assert strategy.name_task(Task("Abc123")) == "wf1_Abc123"


def test_name_provision(strategy: NamingStrategy) -> None:
    assert strategy.name_provision(Provision("p1X", set(), set())) == "wf1_p1X"


@pytest.mark.parametrize(("occurrence", "iteration", "expectation"), [
    (None, (), ("wf1_tA5k_pre", "wf1_tA5k_post")),
    (None, None, ("wf1_tA5k_pre", "wf1_tA5k_post")),
    (None, (0, 1, 2), ("wf1_tA5k_pre_1_2_3", "wf1_tA5k_post_1_2_3")),
    (1, (0, 1, 2), ("wf1_tA5k_pre_2_1_2_3", "wf1_tA5k_post_2_1_2_3")),
    (1, (), ("wf1_tA5k_pre_2", "wf1_tA5k_post_2"))
])
def test_name_checkpoint(strategy: NamingStrategy, occurrence: Optional[int], iteration: Iteration,
                         expectation: Tuple[str, str]) -> None:
    ti = TaskInstance(TaskOccurrence(Task("tA5k"), occurrence), iteration)
    before = strategy.name_checkpoint(Checkpoint(ti, CheckpointType.PRE))
    assert before == expectation[0]
    after = strategy.name_checkpoint(Checkpoint(ti, CheckpointType.POST))
    assert after == expectation[1]


def test_name_checkpoint_sets(strategy: NamingStrategy) -> None:
    task = Task("tA5k")
    before = strategy.name_checkpoint_set(task, CheckpointType.PRE)
    assert before == "wf1_checkpoints_tA5k_pre"
    after = strategy.name_checkpoint_set(task, CheckpointType.POST)
    assert after == "wf1_checkpoints_tA5k_post"


def test_name_checkpoint_sets_unfiltered(strategy: NamingStrategy) -> None:
    task = Task("tA5k")
    before = strategy.name_checkpoint_set_unfiltered(task, CheckpointType.PRE)
    assert before == "wf1_checkpoints_tA5k_pre_all"
    after = strategy.name_checkpoint_set_unfiltered(task, CheckpointType.POST)
    assert after == "wf1_checkpoints_tA5k_post_all"


def test_name_reachability_array(strategy: NamingStrategy) -> None:
    assert strategy.name_reachability_array() == "wf1_reachable"


def test_name_checkpoint_task_inst_map(strategy: NamingStrategy) -> None:
    assert strategy.name_checkpoint_task_inst_map() == "wf1_task_inst_of_checkpoint"


def test_name_task_inst_task_map(strategy: NamingStrategy) -> None:
    assert strategy.name_task_inst_task_map() == "wf1_task_of_inst"


def test_name_task_task_inst_map(strategy: NamingStrategy) -> None:
    assert strategy.name_task_task_inst_map(Task("FooBar")) == "wf1_FooBar_insts"


def test_name_reachable_tasks_function(strategy: NamingStrategy) -> None:
    assert strategy.name_reachable_tasks_function() == "wf1_reachable_tasks"


def test_name_reachable_checkpoints_function(strategy: NamingStrategy) -> None:
    assert strategy.name_reachable_checkpoints_function() == "wf1_reachable_checkpoints"


def test_name_all_checkpoints_set(strategy: NamingStrategy) -> None:
    assert strategy.name_all_checkpoints_set() == "wf1_all_checkpoints"


def test_name_is_index_of_predicate(strategy: NamingStrategy) -> None:
    assert strategy.name_is_index_of_predicate() == "wf1_is_index_of"


def test_name_task_inst_is_predecessor_predicate(strategy: NamingStrategy) -> None:
    assert strategy.name_task_inst_is_predecessor_predicate() == "wf1_task_inst_directly_precede"


def test_name_category_changed_predicate(strategy: NamingStrategy) -> None:
    assert strategy.name_category_changed_predicate() == "wf1_category_changed"


def test_name_count_category_changes_function(strategy: NamingStrategy) -> None:
    assert strategy.name_count_category_changes_function() == "wf1_count_category_changes"


def test_name_count_category_changes_within_task_function(strategy: NamingStrategy) -> None:
    assert strategy.name_count_category_changes_within_task_function() == "wf1_count_category_changes_within_task"


def test_name_category_changes_variable(strategy: NamingStrategy) -> None:
    assert strategy.name_category_changes_variable("provider") == "wf1_provider_changes"


def test_name_all_category_changes_variable(strategy: NamingStrategy) -> None:
    assert strategy.name_all_category_changes_variable() == "wf1_all_category_changes"
