from qosagg.util.combination_generator import generate_combinations


def test_generate_with_zero_counts():
    # given 0s as counts
    counts = [1, 2, 0, 3]

    # when generating values
    combinations = list(generate_combinations(counts))

    # then there are no combinations
    assert combinations == []


def test_generate_with_empty_counts():
    # given only 0s as counts
    counts = []

    # when generating values
    combinations = list(generate_combinations(counts))

    # then there only is the empty combinations
    assert combinations == [()]


def test_generate_with_proper_counts():
    # given only 0s as counts
    counts = [2, 1, 4]

    # when generating values
    combinations = list(generate_combinations(counts))

    # then there are exactly the expected combinations
    assert len(combinations) == 2 * 4
    assert combinations[0] == (0, 0, 0)
    assert combinations[1] == (0, 0, 1)
    assert combinations[2] == (0, 0, 2)
    assert combinations[3] == (0, 0, 3)
    assert combinations[4] == (1, 0, 0)
    assert combinations[5] == (1, 0, 1)
    assert combinations[6] == (1, 0, 2)
    assert combinations[7] == (1, 0, 3)
