from typing import Sequence, overload, TYPE_CHECKING, Callable

from qosagg.domain.graph import Graph
from qosagg.domain.unrolling.task_instance import Iteration

if TYPE_CHECKING:
    from qosagg.domain.unrolling.unrolled_graph import UGraph


class _CompositeUnrollingSequence(Sequence["UGraph"]):
    def __init__(self, subgraphs: Sequence[Graph], iteration: Iteration,
                 unroll: Callable[[Graph, Iteration], "UGraph"]) -> None:
        self.subgraphs = subgraphs
        self.iteration = iteration
        self.unroll = unroll

    @overload
    def __getitem__(self, i: int) -> "UGraph":
        ...

    @overload
    def __getitem__(self, s: slice) -> Sequence["UGraph"]:
        ...

    def __getitem__(self, i):
        if isinstance(i, int):
            return self.unroll(self.subgraphs[i], self.iteration)
        elif isinstance(i, slice):
            return _CompositeUnrollingSequence(self.subgraphs[i], self.iteration, self.unroll)
        raise TypeError("Only integer and slice based indexing is supported")

    def __len__(self) -> int:
        return len(self.subgraphs)
