from typing import Sequence, overload, TYPE_CHECKING, Callable

from qosagg.domain.graph import Graph
from qosagg.domain.unrolling.task_instance import Iteration

if TYPE_CHECKING:
    from qosagg.domain.unrolling.unrolled_graph import UGraph


class _LoopUnrollingSequence(Sequence["UGraph"]):
    def __init__(self, subgraph: Graph, iteration: Iteration, subiterations: Sequence[int],
                 unroll: Callable[[Graph, Iteration], "UGraph"]) -> None:
        self.subgraph = subgraph
        self.iteration = iteration
        self.subiterations = subiterations
        self.unroll = unroll

    @overload
    def __getitem__(self, i: int) -> "UGraph":
        ...

    @overload
    def __getitem__(self, s: slice) -> Sequence["UGraph"]:
        ...

    def __getitem__(self, i):
        if isinstance(i, int):
            subiteration = self.subiterations[i]
            return self.unroll(self.subgraph, (*self.iteration, subiteration))
        elif isinstance(i, slice):
            return _LoopUnrollingSequence(self.subgraph, self.iteration, self.subiterations[i], self.unroll)
        raise TypeError("Only integer and slice based indexing is supported")

    def __len__(self) -> int:
        return len(self.subiterations)
