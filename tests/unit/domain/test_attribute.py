from qosagg.domain.attribute import Attribute


def test_Attribute_eq() -> None:
    # given two equal attributes
    a1 = Attribute("attr", "foo")
    a2 = Attribute("attr", "foo", None)

    # then they should equal
    assert a1 == a2


def test_Attribute_neq() -> None:
    # given two non-equal attributes
    a = Attribute("attr", "int", "5")
    a_name = Attribute("attr2", a.value_type, a.default)
    a_type = Attribute(a.name, "foo2", a.default)
    a_default = Attribute(a.name, a.value_type, None)

    # then they should not equal
    assert a != a_name
    assert a != a_type
    assert a != a_default
    assert a != "foobar"


def test_Attribute_str() -> None:
    a = Attribute("attr", "int", "6")
    assert str(a) == "attribute attr of int default 6"
