from abc import abstractmethod, ABC
from enum import Enum, unique, auto
from typing import List, Set, TypeVar, NewType, Optional

Task = NewType("Task", str)
T = TypeVar('T')


class TaskOccurrence:
    def __init__(self, task: Task, occurrence: Optional[int] = None):
        self.task: Task = task
        self.occurrence: Optional[int] = occurrence
        """The occurrence index of the task in the workflow. None signals that there only is this occurrence of the
        task. """

    def __str__(self) -> str:
        return f"{self.task}_{self.occurrence}"

    def __hash__(self) -> int:
        return hash((self.task, self.occurrence))

    def __eq__(self, other) -> bool:
        if not isinstance(other, TaskOccurrence):
            return False
        return (self.task, self.occurrence) == (other.task, other.occurrence)

    @property
    def is_sole_task_occurrence(self) -> bool:
        return self.occurrence is None

    @is_sole_task_occurrence.setter
    def is_sole_task_occurrence(self, val: bool) -> None:
        if self.is_sole_task_occurrence == val:
            return
        self.occurrence = None if val else 0


class Graph(ABC):
    @abstractmethod
    def get_tasks(self) -> Set[Task]:
        raise NotImplementedError()

    @abstractmethod
    def __eq__(self, o: object) -> bool:
        raise NotImplementedError()

    def __ne__(self, o: object) -> bool:
        return not self.__eq__(o)

    @abstractmethod
    def __str__(self) -> str:
        pass


class SingletonGraph(Graph):
    def __init__(self, task_occ: TaskOccurrence):
        super(SingletonGraph, self).__init__()
        self.task_occ = task_occ

    def get_tasks(self) -> Set[Task]:
        return {self.task_occ.task}

    def __eq__(self, o: object) -> bool:
        return isinstance(o, SingletonGraph) and self.task_occ == o.task_occ

    def __str__(self) -> str:
        return self.task_occ.task


@unique
class GraphComposition(Enum):
    PARALLEL = auto()
    SEQUENTIAL = auto()
    CHOICE = auto()

    def __str__(self):
        return {
            GraphComposition.PARALLEL: '|',
            GraphComposition.SEQUENTIAL: '→',
            GraphComposition.CHOICE: '+',
        }.get(self, super.__str__(self))


class CompositeGraph(Graph):
    def __init__(self, subgraphs: List[Graph], composition: GraphComposition, name: str):
        self.subgraphs = subgraphs
        self.composition = composition
        self.name = name

    def get_tasks(self) -> Set[Task]:
        return {task for subgraph in self.subgraphs for task in subgraph.get_tasks()}

    def __eq__(self, o: object) -> bool:
        return isinstance(o, CompositeGraph) and self.subgraphs == o.subgraphs and self.composition == o.composition \
               and self.name == o.name

    def __str__(self) -> str:
        return "(" + (f" {self.composition} ".join([str(subgraph) for subgraph in self.subgraphs])) + ")=" + self.name


LoopCount = int


class LoopedGraph(Graph):
    def __init__(self, subgraph: Graph, count: LoopCount, name: str):
        self.subgraph = subgraph
        self.count = count
        self.name = name

    def get_tasks(self) -> Set[Task]:
        return self.subgraph.get_tasks()

    def __eq__(self, o):
        return isinstance(o, LoopedGraph) and self.subgraph == o.subgraph and self.count == o.count \
               and self.name == o.name

    def __str__(self):
        return f"({self.subgraph})^{self.count}={self.name}"
