from collections import defaultdict
from typing import List, Mapping, Dict

from lazy import lazy

from qosagg.domain.graph import Task
from qosagg.domain.task_instance_visitor import TaskInstanceVisitor
from qosagg.domain.unrolling.task_instance import Checkpoint
from qosagg.domain.unrolling.unrolled_graph import UGraph, TaskInstance


class GraphCollector:
    def __init__(self, graph: UGraph):
        self.graph = graph

    @lazy
    def task_instances_by_task(self) -> Mapping[Task, List[TaskInstance]]:
        instances: Dict[Task, List[TaskInstance]] = defaultdict(lambda: [])

        def visit_taskinst(ti: TaskInstance):
            instances[ti.task_occ.task] += [ti]

        TaskInstanceVisitor(visit_taskinst).visit(self.graph)
        return instances

    @property
    def task_instances(self) -> List[TaskInstance]:
        grouped: Mapping[Task, List[TaskInstance]] = self.task_instances_by_task
        return [ti for t in grouped for ti in grouped[t]]

    @lazy
    def checkpoints(self) -> List[Checkpoint]:
        return [cp for ti in self.task_instances for cp in ti.checkpoints]
