import os
import subprocess
from typing import Optional


class MinizincExecutor:
    def __init__(self, mzn_executable: str = "minizinc", fail_on_stderr: bool = False):
        self.mzn_exec = mzn_executable
        self.fail_on_stderr = fail_on_stderr

    def solve(self, file: os.PathLike, mzn_args: Optional[str] = None) -> str:
        command = [self.mzn_exec] + (mzn_args.split(" ") if mzn_args is not None else []) + [str(file)]
        proc: subprocess.CompletedProcess = subprocess.run(command, text=True, stdout=subprocess.PIPE,
                                                           stderr=subprocess.PIPE, check=True)
        if proc.stderr != "" and self.fail_on_stderr:
            raise RuntimeError("MiniZinc wrote to stderr", proc.stderr)
        return proc.stdout
