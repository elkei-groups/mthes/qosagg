lexer grammar WorkflowLexer;

// fragments
fragment DIGIT : [0-9];
fragment LOWERCASE : [a-z] ;
fragment UPPERCASE : [A-Z] ;
fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
fragment LETTER : LOWERCASE | UPPERCASE;
fragment NEWLINE : ('\r'? '\n' | '\r');

// keywords
PROVISION : P R O V I S I O N;
FOR : F O R;
GRAPH : G R A P H;
AGGREGATION : A G G R E G A T I O N;
ATTRIBUTE : A T T R I B U T E;
WORKFLOW : W O R K F L O W;
OF : O F;
NULL : N U L L;
DEFAULT : D E F A U L T;
CATEGORY : C A T E G O R Y;
MINIZINC: M I N I Z I N C;

// non-keyword bigger words
ID : LETTER (DIGIT | LETTER | '_')*;
INT : '-'? DIGIT+;
FLOAT : INT '.' DIGIT+;

// "single" characters
IS : '==' | '=';
COMMA : ',';
COLON : ':';
SEMICOLON : ';';
RARROW : '→' | '->';
PARALLEL : '∥' | '|';
CHOICE : '+';
LPARENTHESES : '(';
RPARENTHESES : ')';
LBRACE : '{';
RBRACE : '}';
EXP : '^';
QUESTIONMARK : '?';

// skipped tokens
WHITESPACE : (' ' | '\t' | NEWLINE)+ -> channel(HIDDEN);
SINGLELINECOMMENTS : '%' ~('\r' | '\n')* -> skip;

ANYTHING : .+?;