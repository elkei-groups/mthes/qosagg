import abc
from typing import List, Iterable

from qosagg.domain.aggregation import Aggregation
from qosagg.domain.graph import Task
from qosagg.domain.provision import Provision
from qosagg.domain.unrolling.task_instance import Checkpoint, CheckpointType
from qosagg.domain.unrolling.unrolled_graph import TaskInstance, CompositeUGraph


class NamingStrategy(abc.ABC):
    def name_provision_enum(self) -> str:
        return self.name_typedef("provision")

    def name_task_enum(self) -> str:
        return self.name_typedef("task")

    def name_checkpoint_enum(self) -> str:
        return self.name_typedef("checkpoint")

    def name_task_instance_enum(self) -> str:
        return self.name_typedef("task_inst")

    @abc.abstractmethod
    def name_typedef(self, type_name: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_selection_attribute(self, task_inst: TaskInstance, attribute: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_selection_array(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_attribute_array(self, attribute_name: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_provision_names_array(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_reachability_array(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def name_reachable_tasks_function(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def name_reachable_task_instances_function(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def name_reachable_checkpoints_function(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def name_checkpoint_array(self, attribute_name: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_checkpoint_set(self, task: Task, cp_type: CheckpointType) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_checkpoint_set_unfiltered(self, task: Task, cp_type: CheckpointType) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_checkpoint_task_inst_map(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_task_inst_task_map(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_task_task_inst_map(self, task: Task) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_aggregation(self, aggregation: Aggregation) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_composite(self, composite: CompositeUGraph) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_composite_inst_array(self, composite_name: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_task(self, task: Task) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_task_instance(self, task_inst: TaskInstance) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_provision(self, provision: Provision) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_checkpoint(self, checkpoint: Checkpoint) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_all_checkpoints_set(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_provisions_for_task_set(self, task: Task) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_is_index_of_predicate(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_task_inst_is_predecessor_predicate(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_category_changed_predicate(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_count_category_changes_function(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_count_category_changes_within_task_function(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_category_changes_variable(self, name: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def name_all_category_changes_variable(self) -> str:
        raise NotImplementedError()


class PrefixNamingStrategy(NamingStrategy):
    def __init__(self, workflow_name: str):
        self.workflow_name = workflow_name

    def name_selection_attribute(self, task_inst: TaskInstance, attribute: str) -> str:
        return self._get_prefixed(task_inst.task_occ.task + "_" + attribute +
                                  self._get_instance_suffix(task_inst))

    def name_typedef(self, type_name: str) -> str:
        return self._get_prefixed(type_name).upper()

    def name_selection_array(self) -> str:
        return self._get_prefixed("selection")

    def name_attribute_array(self, attribute_name: str) -> str:
        return self._get_prefixed(attribute_name)

    def name_provision_names_array(self) -> str:
        return self._get_prefixed("provision_names")

    def name_provisions_for_task_set(self, task: Task) -> str:
        return self._get_prefixed(task + "_provisions")

    def name_reachability_array(self):
        return self._get_prefixed("reachable")

    def name_reachable_tasks_function(self):
        return self._get_prefixed("reachable_tasks")

    def name_reachable_task_instances_function(self):
        return self._get_prefixed("reachable_task_insts")

    def name_reachable_checkpoints_function(self):
        return self._get_prefixed("reachable_checkpoints")

    def name_checkpoint_array(self, attribute_name: str) -> str:
        return self._get_prefixed("checkpoints_" + attribute_name)

    def name_checkpoint_set(self, task: Task, cp_type: CheckpointType) -> str:
        return self._get_prefixed(f"checkpoints_{task}_{self._name_cp_type(cp_type)}")

    def name_all_checkpoints_set(self) -> str:
        return self._get_prefixed("all_checkpoints")

    def name_checkpoint_set_unfiltered(self, task: Task, cp_type: CheckpointType) -> str:
        return self.name_checkpoint_set(task, cp_type) + "_all"

    def name_checkpoint_task_inst_map(self) -> str:
        return self._get_prefixed("task_inst_of_checkpoint")

    def name_task_inst_task_map(self) -> str:
        return self._get_prefixed("task_of_inst")

    def name_task_task_inst_map(self, task: Task) -> str:
        return self._get_prefixed(task + "_insts")

    def name_aggregation(self, aggregation: Aggregation) -> str:
        return self._get_prefixed("aggregated_" + aggregation.attribute_name)

    def name_checkpoint(self, checkpoint: Checkpoint) -> str:
        name = f"{checkpoint.task_instance.task_occ.task}_{self._name_cp_type(checkpoint.cp_type)}" + \
               self._get_instance_suffix(checkpoint.task_instance)
        return self._get_prefixed(name)

    def name_composite(self, composite: CompositeUGraph) -> str:
        return self._get_prefixed(composite.name + self._get_counters_suffix(composite.iteration))

    def name_composite_inst_array(self, composite_name: str) -> str:
        return self._get_prefixed(composite_name + "_insts")

    def name_task(self, task: Task) -> str:
        return self._get_prefixed(task)

    def name_task_instance(self, task_inst: TaskInstance) -> str:
        return self._get_prefixed(task_inst.task_occ.task + "_inst" + self._get_instance_suffix(task_inst))

    def name_provision(self, provision: Provision) -> str:
        return self._get_prefixed(provision.name)

    def name_is_index_of_predicate(self) -> str:
        return self._get_prefixed("is_index_of")

    def name_task_inst_is_predecessor_predicate(self) -> str:
        return self._get_prefixed("task_inst_directly_precede")

    def name_category_changed_predicate(self) -> str:
        return self._get_prefixed("category_changed")

    def name_count_category_changes_function(self) -> str:
        return self._get_prefixed("count_category_changes")

    def name_count_category_changes_within_task_function(self) -> str:
        return self._get_prefixed("count_category_changes_within_task")

    def name_category_changes_variable(self, name: str) -> str:
        return self._get_prefixed(name + "_changes")

    def name_all_category_changes_variable(self) -> str:
        return self._get_prefixed("all_category_changes")

    def _get_prefixed(self, name: str) -> str:
        return f"{self.workflow_name}_{name}"

    @classmethod
    def _get_instance_suffix(cls, task_inst: TaskInstance) -> str:
        occurrence_counters: List[int] = [] if task_inst.task_occ.occurrence is None \
            else [task_inst.task_occ.occurrence]
        iteration_counters: List[int] = [] if task_inst.iteration is None else list(task_inst.iteration)
        counters: List[int] = occurrence_counters + iteration_counters
        return cls._get_counters_suffix(counters)

    @classmethod
    def _get_counters_suffix(cls, counters: Iterable) -> str:
        return "".join("_" + str(c + 1) for c in counters)

    @classmethod
    def _name_cp_type(cls, cp_type: CheckpointType) -> str:
        return {
            CheckpointType.PRE: "pre",
            CheckpointType.POST: "post"
        }[cp_type]
