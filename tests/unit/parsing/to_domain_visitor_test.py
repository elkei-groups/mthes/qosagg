import textwrap
from typing import List, Callable, TypeVar

import pytest
from antlr4 import InputStream, CommonTokenStream

from qosagg.domain.aggregation import Aggregation
from qosagg.domain.attribute import Attribute
from qosagg.domain.category import Category
from qosagg.domain.graph import Graph, CompositeGraph, GraphComposition, LoopedGraph, Task, TaskOccurrence, \
    SingletonGraph
from qosagg.domain.provision import AttributeValue, Provision
from qosagg.domain.workflow import Workflow, NamedWorkflow
from qosagg.parsing.generated.WorkflowLexer import WorkflowLexer
from qosagg.parsing.generated.WorkflowParser import WorkflowParser
from qosagg.parsing.to_domain_visitor import ToDomainVisitor, MiniZincBlock
from tests.helpers import ssg
from tests.unit.parsing.failing_error_listener import FailingErrorListener

_T = TypeVar("_T")
VisitInParsingResultFixture = Callable[[str, Callable[[WorkflowParser], _T]], _T]


@pytest.fixture
def visit_in_parsing_result() -> VisitInParsingResultFixture:
    def parser_for_string(text: str) -> WorkflowParser:
        lexer = WorkflowLexer(InputStream(text))
        parser = WorkflowParser(CommonTokenStream(lexer))
        parser.addErrorListener(FailingErrorListener())
        return parser

    return lambda text, parsing: ToDomainVisitor().visit(parsing(parser_for_string(text)))


def test_visit_workflow(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a simple workflow
    text = """
    provision p1 for A, B: cost = 5, provider = SkyBlue;
    graph: A -> B → C;
    provision p2 for B, C: cost = 4, provider = EhDabuEs;
    category provider;
    aggregation cost: sum, sum;"""

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.parseWorkflow())

    # then the result should be a workflow that contains a graph with 3 tasks and two provisions
    assert isinstance(actual, Workflow)
    assert len(actual.graph.get_tasks()) == 3
    assert len(actual.provisions) == 2
    assert len(actual.aggregations) == 1
    assert len(actual.categories) == 1


def test_visit_graph_multiple_linear(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a linear graph
    text = "A -> B1 → b2 Delta"

    # when parsing and transforming it
    actual: Graph = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should contain the expected tasks
    assert actual.get_tasks() == {"A", "B1", "b2", "Delta"}
    assert isinstance(actual, CompositeGraph)
    assert actual.composition == GraphComposition.SEQUENTIAL


@pytest.mark.parametrize(["composition_string", "composition_type"], [
    ("|", GraphComposition.PARALLEL),
    ("+", GraphComposition.CHOICE),
])
def test_visit_graph_multiple_parallel_or_choice(visit_in_parsing_result, composition_string: str,
                                                 composition_type: GraphComposition):
    # given a composite graph
    text = f"A B {composition_string} C D"

    # when parsing and transforming it
    actual: Graph = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should contain the expected tasks and type
    assert actual.get_tasks() == {"A", "B", "C", "D"}
    assert isinstance(actual, CompositeGraph)
    assert actual.composition == composition_type


def test_visit_graph_precedence_parentheses(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a graph with parentheses changing precedence
    text = "(C | B) A"

    # when parsing and transforming it
    actual: Graph = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should have top-level type sequence and the right part should be the singleton A
    assert isinstance(actual, CompositeGraph)
    assert actual.composition == GraphComposition.SEQUENTIAL
    assert actual.subgraphs[-1] == ssg("A")


def test_visit_graph_precedence_noparentheses(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a graph with parentheses changing precedence
    text = "A B ^ 3 + C D | E F + G ^ 4 H"

    # when parsing and transforming it
    actual: Graph = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should have top level |, then + and lowest level ->
    assert actual == CompositeGraph([
        CompositeGraph([
            CompositeGraph([
                ssg("A"),
                LoopedGraph(ssg("B"), 3, "loop1")
            ], GraphComposition.SEQUENTIAL, "sequence1"),
            CompositeGraph([
                ssg("C"),
                ssg("D")
            ], GraphComposition.SEQUENTIAL, "sequence2")
        ], GraphComposition.CHOICE, "choice1"),
        CompositeGraph([
            CompositeGraph([
                ssg("E"),
                ssg("F")
            ], GraphComposition.SEQUENTIAL, "sequence3"),
            CompositeGraph([
                LoopedGraph(ssg("G"), 4, "loop2"),
                ssg("H")
            ], GraphComposition.SEQUENTIAL, "sequence4")
        ], GraphComposition.CHOICE, "choice2")
    ], GraphComposition.PARALLEL, "parallel1")


def test_visit_graph_single(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a singleton graph
    text = "A"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should only contain that single task
    expected = ssg("A")
    assert actual == expected


def test_visit_graph_looped(visit_in_parsing_result: VisitInParsingResultFixture):
    # given the text of a looped graph with task A and 7 iterations
    text = "A ^ 7"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should equal a looped graph with task A and 7 iterations
    expected = LoopedGraph(ssg("A"), 7, "loop1")
    assert actual == expected


def test_visit_null_graph(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a null graph
    text = "null"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should be none
    assert actual == CompositeGraph([], GraphComposition.SEQUENTIAL, "null1")


def test_visit_optional_graph(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a graph with optionals
    text = "(A + B?)?"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the returned graph should reflect the optionals with choices
    assert actual == CompositeGraph([
        CompositeGraph([
            ssg("A"),
            CompositeGraph([
                ssg("B"),
                CompositeGraph([], GraphComposition.SEQUENTIAL, "null1")
            ], GraphComposition.CHOICE, "optional1")
        ], GraphComposition.CHOICE, "choice1"),
        CompositeGraph([], GraphComposition.SEQUENTIAL, "null2")
    ], GraphComposition.CHOICE, "optional2")


def test_visit_provision(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid list of attribute equations
    text = "provision foobar for foo, bar: cost = -5.5, latency = 2;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.provision())

    # then the provision should have right name, tasks and attributes
    expected = Provision("foobar", {Task("foo"), Task("bar")},
                         {AttributeValue("cost", "-5.5"), AttributeValue("latency", "2")})
    assert actual == expected


def test_visit_provision_without_attribute(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid provision without attributes
    text = "provision bar for foo;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.provision())

    # then the provision should have right name, tasks and no attributes
    expected = Provision("bar", {Task("foo")}, set())
    assert actual == expected


def test_visit_attribute_eqs_multiple(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid list of attribute equations
    text = "foo = 1, bar == 2.0"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeEqs())

    # then the attributes should have right names and values
    expected = [AttributeValue("foo", "1"), AttributeValue("bar", "2.0")]
    assert actual == expected


def test_visit_attribute_eqs_multiple_with_comma(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid list of attribute equations with commas in the attribute values
    text = "foo = myarray[row, (column + 1)], bar = if x then 3.0 else 4 endif, lastattr = lastval"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeEqs())

    # then the Attributes should have the right names and values
    expected = [AttributeValue("foo", "myarray[row, (column + 1)]"),
                AttributeValue("bar", "if x then 3.0 else 4 endif"), AttributeValue("lastattr", "lastval")]
    assert actual == expected


def test_visit_attribute_value_with_whitespace(visit_in_parsing_result: VisitInParsingResultFixture):
    # given an attribute value with whitespace inside
    expected = "if myArray[123] == 3.0 then CONST_FOO else bar(7) endif"
    text = expected + "% some comment"

    # when parsing it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeValue())

    # then the text should be exactly what we entered but without the comment
    assert actual == expected


def test_visit_attribute_eqs_single(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid attribute equation
    text = "foo = -5"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeEqs())

    # then the attribute should have right name and value and be the only returned attribute
    expected = [AttributeValue("foo", "-5")]
    assert actual == expected


def test_visit_attribute_eq(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid AttributeValue equation
    text = "fooBar = 1.3"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeEq())

    # then the attribute should have right name and value
    expected = AttributeValue("fooBar", "1.3")
    assert actual == expected


def test_visit_aggregation_two_aggregators(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid aggregation
    text = "aggregation testAgg: sum_pow4, fooBar;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.aggregation())

    # then the aggregation should have right attribute name and sequential aggregator name
    expected = Aggregation("testAgg", "sum_pow4", "fooBar")
    assert actual == expected


def test_visit_aggregation_one_aggregator(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid aggregation
    text = "aggregation testAgg: sum_pow4;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.aggregation())

    # then the aggregation should have right attribute name and sequential aggregator name
    expected = Aggregation("testAgg", "sum_pow4", "sum_pow4")
    assert actual == expected


def test_visit_attribute_declaration_with_name_only(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid attribute with only the name
    text = "attribute testAttr;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeDecl())

    # then the attribute should match the expectation
    expected = Attribute("testAttr", "float")
    assert actual == expected


def test_visit_attribute_declaration_with_type(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid attribute with type
    text = "attribute testAttr of int;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeDecl())

    # then the attribute should match the expectation
    expected = Attribute("testAttr", "int")
    assert actual == expected


def test_visit_attribute_declaration_with_default(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid attribute with default
    text = "attribute testAttr default 5.0;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeDecl())

    # then the attribute should match the expectation
    expected = Attribute("testAttr", "float", "5.0")
    assert actual == expected


def test_visit_attribute_declaration_with_type_and_default(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a valid attribute with type and default
    text = "attribute testAttr of int default 5;"

    # when parsing and transforming it
    actual = visit_in_parsing_result(text, lambda parser: parser.attributeDecl())

    # then the attribute should match the expectation
    expected = Attribute("testAttr", "int", "5")
    assert actual == expected


def test_naming_strategy(visit_in_parsing_result: VisitInParsingResultFixture):
    # given two workflows and the second has the name that is expected for the first to be generated
    text = "workflow { graph: A; }; workflow wf1 { graph: A; }; workflow wf1 { graph: A; }; workflow wf1 { graph: A; };"

    # when parsing and transforming it
    workflows: List[NamedWorkflow] = visit_in_parsing_result(text, lambda parser: parser.parseTLDs())

    # then the first name should be as expected and the second should be adapted to not shadow the first one
    assert len(workflows) == 4
    assert workflows[0].name == "wf"
    assert workflows[1].name == "wf1"
    assert workflows[2].name == "wf12"
    assert workflows[3].name == "wf13"


def test_visit_category(visit_in_parsing_result: VisitInParsingResultFixture):
    # given a simple category declaration
    text = "category provider;"

    # when parsing and transforming it
    category: Category = visit_in_parsing_result(text, lambda parser: parser.category())

    # then the category should equal our expectation
    assert category == Category("provider")


def test_multiple_task_occurrences(visit_in_parsing_result: VisitInParsingResultFixture) -> None:
    # given a graph with two occurrences of task A
    text = "A → B → A"

    # when parsing and transforming it
    graph: Graph = visit_in_parsing_result(text, lambda parser: parser.graph())

    # then the two occurrences should be indexed correctly
    assert isinstance(graph, CompositeGraph)
    assert len(graph.subgraphs) == 2
    inner_seq = graph.subgraphs[0]
    a1 = graph.subgraphs[1]
    assert isinstance(inner_seq, CompositeGraph)
    assert len(inner_seq.subgraphs) == 2
    a0 = inner_seq.subgraphs[0]
    b = inner_seq.subgraphs[1]
    assert a0 == SingletonGraph(TaskOccurrence(Task("A"), 0))
    assert a1 == SingletonGraph(TaskOccurrence(Task("A"), 1))
    assert b == SingletonGraph(TaskOccurrence(Task("B"), None))


def test_mixed_tlds(visit_in_parsing_result: VisitInParsingResultFixture) -> None:
    # given a workflow tld and a minizinc tld
    text = "workflow { graph: A; }; minizinc { some mzn code }"

    # when parsing and transforming it
    tlds = visit_in_parsing_result(text, lambda parser: parser.parseTLDs())

    # then there should be one named workflow and one minizinc block
    assert isinstance(tlds[0], NamedWorkflow)
    assert isinstance(tlds[1], str)


def test_minizinc_block(visit_in_parsing_result: VisitInParsingResultFixture) -> None:
    # given a minizinc block
    text = textwrap.dedent("""\
        minizinc {
        \tvar int: i;
        \tconstraint i < 5;
        }""")

    # when parsing it
    block: MiniZincBlock = visit_in_parsing_result(text, lambda parser: parser.minizincDecl())

    # then there should be the correct code in the result
    expectation = "var int: i;\nconstraint i < 5;"
    assert str(block) == expectation
