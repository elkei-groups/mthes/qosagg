import textwrap
from io import StringIO
from typing import Callable

import pytest

from qosagg.codegen.base_minizinc_generator import MiniZincGeneratorConfig
from qosagg.codegen.checkpoints.checkpoint_generator import CheckpointGenerator
from qosagg.codegen.graph_collector import GraphCollector
from qosagg.codegen.naming_strategy import PrefixNamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.attribute import Attribute
from qosagg.domain.graph import CompositeGraph, LoopedGraph, GraphComposition
from qosagg.domain.unrolling.unrolled_graph import unroll, UGraph
from qosagg.domain.workflow import Workflow
from tests.helpers import ssg


@pytest.fixture()
def config() -> MiniZincGeneratorConfig:
    return MiniZincGeneratorConfig(output=False, checkpoints=True, reachability=False)


@pytest.fixture()
def generate(output: StringIO, config: MiniZincGeneratorConfig) -> Callable[[Workflow], None]:
    def get_generator(w: Workflow[UGraph]) -> CheckpointGenerator:
        return CheckpointGenerator(w, output, config, PrefixNamingStrategy("wf"), GraphCollector(w.graph))

    return lambda w: get_generator(w).generate_checkpoints()


def test_checkpoint_declaration(generate, output):
    # given a nested looped workflow A^2 -> B
    workflow = Workflow(unroll(CompositeGraph([
        LoopedGraph(ssg("A"), 2, "l1"),
        ssg("B")
    ], GraphComposition.SEQUENTIAL, "s1")), [], [Aggregation("cost", "sum", "sum")], [], [Attribute("cost", "euros")])

    # when generating the MiniZinc code
    generate(workflow)

    # then the output should contain a proper aggregation for cost
    text = output.getvalue()
    print("\n" + text)
    enum_exp = "enum WF_CHECKPOINT = { wf_A_pre_1, wf_A_post_1, wf_A_pre_2, wf_A_post_2, wf_B_pre, wf_B_post };\n"
    array_exp = "array[WF_CHECKPOINT] of var euros: wf_checkpoints_cost;\n"
    set_a_pre_exp = "set of WF_CHECKPOINT: wf_checkpoints_A_pre = { wf_A_pre_1, wf_A_pre_2 };\n"
    set_a_post_exp = "set of WF_CHECKPOINT: wf_checkpoints_A_post = { wf_A_post_1, wf_A_post_2 };\n"
    set_b_pre_exp = "set of WF_CHECKPOINT: wf_checkpoints_B_pre = { wf_B_pre };\n"
    set_b_post_exp = "set of WF_CHECKPOINT: wf_checkpoints_B_post = { wf_B_post };\n"
    tasks_exp = "array[WF_CHECKPOINT] of WF_TASK_INST: wf_task_inst_of_checkpoint = " \
                "[wf_A_inst_1, wf_A_inst_1, wf_A_inst_2, wf_A_inst_2, wf_B_inst, wf_B_inst];\n"
    assert enum_exp in text
    assert array_exp in text
    assert set_a_pre_exp in text
    assert set_a_post_exp in text
    assert set_b_pre_exp in text
    assert set_b_post_exp in text
    assert tasks_exp in text


def test_reachability_filter_generation(generate, output, config):
    # given a mixed workflow (A + B)
    choice = CompositeGraph([
        ssg("A"),
        ssg("B")
    ], GraphComposition.CHOICE, "c1")
    workflow = Workflow(unroll(choice), [], [], [], [])

    # when generating the MiniZinc code
    config.reachability = True
    generate(workflow)

    # then the output should contain the reachabilities assignment
    text = output.getvalue()
    filtering_exp = textwrap.dedent("""
        set of WF_CHECKPOINT: wf_checkpoints_B_pre_all = { wf_B_pre };
        var set of WF_CHECKPOINT: wf_checkpoints_B_pre = wf_reachable_checkpoints(wf_checkpoints_B_pre_all);
        set of WF_CHECKPOINT: wf_checkpoints_B_post_all = { wf_B_post };
        var set of WF_CHECKPOINT: wf_checkpoints_B_post = wf_reachable_checkpoints(wf_checkpoints_B_post_all);
        """)
    assert filtering_exp in text


@pytest.mark.parametrize("reachability", [True, False])
def test_all_checkpoints_generation(generate, output: StringIO, config: MiniZincGeneratorConfig, reachability: bool):
    # given a simple workflow with one tasks: A
    wf = Workflow(unroll(ssg("A")), [], [], [], [])

    # when generating the MiniZinc code
    config.reachability = reachability
    generate(wf)

    # then the output should contain the correct set with all the checkpoints
    text = output.getvalue()
    all_cp_exp = "var set of WF_CHECKPOINT: wf_all_checkpoints = wf_reachable_checkpoints(WF_CHECKPOINT);\n" \
        if reachability else "set of WF_CHECKPOINT: wf_all_checkpoints = WF_CHECKPOINT;\n"
    assert all_cp_exp in text
