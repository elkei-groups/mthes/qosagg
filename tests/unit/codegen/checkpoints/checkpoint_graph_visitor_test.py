from io import StringIO

import pytest
from pytest_mock import MockerFixture

from qosagg.codegen.checkpoints.checkpoint_graph_visitor import CheckpointGraphVisitor
from qosagg.codegen.naming_strategy import PrefixNamingStrategy
from qosagg.domain.aggregation import Aggregation
from qosagg.domain.graph import CompositeGraph, GraphComposition, LoopedGraph
from qosagg.domain.unrolling.unrolled_graph import unroll
from tests.helpers import ssg


@pytest.fixture
def visitor(output, mocker: MockerFixture) \
        -> CheckpointGraphVisitor:
    aggregation = Aggregation("atr", "agg", "ignored")
    mocker.patch("qosagg.codegen.aggregating_graph_visitor.AggregatingGraphVisitor.visit_task_instance") \
        .side_effect = lambda g: g.task_occ.task
    return CheckpointGraphVisitor(aggregation, PrefixNamingStrategy("wf"), output)


def test_visit_singleton_graph(visitor: CheckpointGraphVisitor, output: StringIO):
    # given a singleton graph
    graph = ssg("A")

    # when generating checkpoints
    visitor.visit(unroll(graph))

    # then the output should contain two aggregations for A
    text = output.getvalue()
    print(text)
    assert "constraint wf_checkpoints_atr[wf_A_pre] = agg([]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_post] = agg([wf_checkpoints_atr[wf_A_pre], wf_A_atr]);\n" in text


def test_visit_sequential_graph(visitor: CheckpointGraphVisitor, output: StringIO):
    # given a sequential composition A -> B -> C
    graph = CompositeGraph([
        CompositeGraph([ssg("A"), ssg("B")], GraphComposition.SEQUENTIAL, "s2"),
        ssg("C")
    ], GraphComposition.SEQUENTIAL, "s1")

    # when generating checkpoints
    visitor.visit(unroll(graph))

    # then the output should contain the expected aggregations
    text = output.getvalue()
    print(text)
    assert "constraint wf_checkpoints_atr[wf_A_pre] = agg([]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_B_pre] = agg([A]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_C_pre] = agg([agg([A, B])]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_post] = agg([wf_checkpoints_atr[wf_A_pre], wf_A_atr]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_B_post] = agg([wf_checkpoints_atr[wf_B_pre], wf_B_atr]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_C_post] = agg([wf_checkpoints_atr[wf_C_pre], wf_C_atr]);\n" in text


@pytest.mark.parametrize("composition, name", [(GraphComposition.CHOICE, "choice1"), (GraphComposition.PARALLEL, "p1")])
def test_visit_mixed_graph(visitor: CheckpointGraphVisitor, output: StringIO, composition: GraphComposition,
                           name: str):
    # given a non sequential composition A -> (B +/| C)
    inner_composite = CompositeGraph([ssg("B"), ssg("C")], composition, name)
    graph = CompositeGraph([
        ssg("A"),
        inner_composite
    ], GraphComposition.SEQUENTIAL, "s1")

    # when generating checkpoints
    visitor.visit(unroll(graph))

    # then the output should contain the expected aggregations
    text = output.getvalue()
    assert "constraint wf_checkpoints_atr[wf_A_pre] = agg([]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_B_pre] = agg([A]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_C_pre] = agg([A]);\n" in text


def test_visit_looped_graph_simple(visitor: CheckpointGraphVisitor, output: StringIO):
    # given a simple looped graph
    graph = LoopedGraph(ssg("A"), 3, "l1")

    # when generating checkpoints
    visitor.visit(unroll(graph))

    # then the output should contain the expected aggregations
    text = output.getvalue()
    print(text)
    assert "constraint wf_checkpoints_atr[wf_A_pre_1] = agg([]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_post_1] = agg([wf_checkpoints_atr[wf_A_pre_1], wf_A_atr_1]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_pre_2] = agg([A]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_post_2] = agg([wf_checkpoints_atr[wf_A_pre_2], wf_A_atr_2]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_pre_3] = agg([A, A]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_post_3] = agg([wf_checkpoints_atr[wf_A_pre_3], wf_A_atr_3]);\n" in text


def test_visit_looped_graph_nested(visitor: CheckpointGraphVisitor, output: StringIO):
    # given a simple looped graph
    graph = LoopedGraph(LoopedGraph(ssg("A"), 3, "l2"), 3, "l1")

    # when generating checkpoints
    visitor.visit(unroll(graph))

    # then the output should contain the expected aggregations
    text = output.getvalue()
    print(text)
    assert "constraint wf_checkpoints_atr[wf_A_pre_1_1] = agg([]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_pre_1_2] = agg([A]);\n" in text
    assert "constraint wf_checkpoints_atr[wf_A_pre_1_3] = agg([A, A]);\n" in text
    # ...
    assert "constraint wf_checkpoints_atr[wf_A_pre_2_2] = agg([agg([A, A, A]), A]);" in text
    assert "constraint wf_checkpoints_atr[wf_A_post_2_2] = agg([wf_checkpoints_atr[wf_A_pre_2_2], wf_A_atr_2_2]);" \
           in text
    # ...
    assert "constraint wf_checkpoints_atr[wf_A_pre_3_3] = agg([agg([A, A, A]), agg([A, A, A]), A, A]);" in text


def test_visit_choice_in_loop(visitor: CheckpointGraphVisitor, output: StringIO):
    # given a graph with a choice in a loop
    choice1 = CompositeGraph([ssg("A"), ssg("B")], GraphComposition.CHOICE, "choice1")
    graph = LoopedGraph(choice1, 2, "l1")

    # when generating aggregations
    visitor.visit(unroll(graph))

    # then the output should contain the expected aggregations
    actual = output.getvalue()
    print(actual)
    assert "constraint wf_checkpoints_atr[wf_A_pre_2] = agg([[A, B][wf_choice1_1]])" in actual


def test_visit_empty_composition(visitor: CheckpointGraphVisitor, output: StringIO):
    # given a graph with empty composition
    graph = CompositeGraph([], GraphComposition.SEQUENTIAL, "s1")

    # when generating aggregations
    visitor.visit(unroll(graph))

    # then the output should contain the empty aggregation
    actual = output.getvalue()
    print(actual)
    assert actual.strip() == ""


def test_visit_choice_of_optionals(visitor: CheckpointGraphVisitor, output: StringIO) -> None:
    # given the graph (A? + B?)^2
    graph = LoopedGraph(CompositeGraph([
        CompositeGraph([
            ssg("A"),
            CompositeGraph([], GraphComposition.SEQUENTIAL, "nullA")
        ], GraphComposition.CHOICE, "optionalA"),
        CompositeGraph([
            ssg("B"),
            CompositeGraph([], GraphComposition.SEQUENTIAL, "nullB")
        ], GraphComposition.CHOICE, "optionalB")
    ], GraphComposition.CHOICE, "choice"), 2, "loop1")

    # when visiting it
    actual = visitor.visit(unroll(graph))

    # then the output should contain the expected aggregations
    actual = output.getvalue()
    print(actual)
    for checkpoint, aggregation in [
        ("wf_A_pre_1", "agg([])"),
        ("wf_A_post_1", "agg([wf_checkpoints_atr[wf_A_pre_1], wf_A_atr_1])"),
        ("wf_B_pre_1", "agg([])"),
        ("wf_B_post_1", "agg([wf_checkpoints_atr[wf_B_pre_1], wf_B_atr_1])"),
        ("wf_A_pre_2", "agg([[[A, agg([])][wf_optionalA_1], [B, agg([])][wf_optionalB_1]][wf_choice_1]])"),
        ("wf_A_post_2", "agg([wf_checkpoints_atr[wf_A_pre_2], wf_A_atr_2])"),
        ("wf_B_pre_2", "agg([[[A, agg([])][wf_optionalA_1], [B, agg([])][wf_optionalB_1]][wf_choice_1]])"),
        ("wf_B_post_2", "agg([wf_checkpoints_atr[wf_B_pre_2], wf_B_atr_2])"),
    ]:
        assert f"constraint wf_checkpoints_atr[{checkpoint}] = {aggregation};" in actual
