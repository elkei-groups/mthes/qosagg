from antlr4.error.ErrorListener import ErrorListener


class ParsingError(Exception):
    def __init__(self, line: int, column: int, msg: str) -> None:
        self.line = line
        self.column = column
        self.msg = msg

    def __str__(self) -> str:
        return f"Parsing failed at {self.line}:{self.column}: {self.msg}"


class ThrowingErrorListener(ErrorListener):
    def syntaxError(self, recognizer, offending_symbol, line, column, msg, e):
        raise ParsingError(line, column, msg)
