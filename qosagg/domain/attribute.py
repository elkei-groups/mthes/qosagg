from typing import Optional, NamedTuple


class Attribute(NamedTuple):
    name: str
    value_type: str = "float"
    default: Optional[str] = None

    def __str__(self) -> str:
        return f"attribute {self.name} of {self.value_type} default {self.default}"
