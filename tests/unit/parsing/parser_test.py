import textwrap
from typing import Callable

import pytest
from antlr4 import InputStream, CommonTokenStream

from qosagg.parsing.generated.WorkflowLexer import WorkflowLexer
from qosagg.parsing.generated.WorkflowParser import WorkflowParser
from tests.unit.parsing.failing_error_listener import FailingErrorListener

ParserForStringFixture = Callable[[str], WorkflowParser]


@pytest.fixture
def parser_for_string() -> ParserForStringFixture:
    def parser_for_string(text: str) -> WorkflowParser:
        lexer = WorkflowLexer(InputStream(text))
        parser = WorkflowParser(CommonTokenStream(lexer))
        parser.addErrorListener(FailingErrorListener())
        return parser

    return parser_for_string


def test_can_parse_provision_with_multiple_tasks_and_attributes(parser_for_string: ParserForStringFixture):
    # given a valid provision string
    provision_text = "provision a1 for A, B: cost = 1, latency = 2;"

    # when parsing the string
    ctx = parser_for_string(provision_text).provision()

    # then the context should contain the right data
    assert ctx.ID().getText() == "a1"
    task_ids_ctx: WorkflowParser.IdsContext = ctx.ids()
    assert [id_ctx.getText() for id_ctx in task_ids_ctx.ID()] == ["A", "B"]
    assert len(ctx.attributeEqs().attributeEq()) == 2


def test_can_parse_provision_with_single_task_and_attribute(parser_for_string: ParserForStringFixture):
    # given a valid provision string
    provision_text = "provision a1 for A: cost = 1;"

    # when parsing the string
    ctx = parser_for_string(provision_text).provision()

    # then the context should contain the right data
    assert ctx.ID().getText() == "a1"
    task_ids_ctx: WorkflowParser.IdsContext = ctx.ids()
    assert [id_ctx.getText() for id_ctx in task_ids_ctx.ID()] == ["A"]
    assert len(ctx.attributeEqs().attributeEq()) == 1


def test_can_parse_provision_without_attributes(parser_for_string: ParserForStringFixture):
    # given a provision without attribute
    text = "provision p for A, B;"

    # when parsing the provision
    ctx = parser_for_string(text).provision()

    # then there should be no attributes but the name and task ids
    assert ctx.ID().getText() == "p"
    assert len(ctx.ids().ID()) == 2
    assert ctx.attributeEqs() is None


def test_can_parse_attribute_eqs_with_commas(parser_for_string) -> None:
    # given attribute equations with commas in the values
    text = "foo = myarray[i, j], bar == somevalue"

    # when parsing the attribute equations
    ctx = parser_for_string(text).attributeEqs()

    # then there should be only too eqs
    assert len(ctx.attributeEq()) == 2
    attr1 = ctx.attributeEq(0)
    assert ctx.parser.getTokenStream().getText(attr1.start, attr1.stop) == "foo = myarray[i, j]"
    attr2 = ctx.attributeEq(1)
    assert ctx.parser.getTokenStream().getText(attr2.start, attr2.stop) == "bar == somevalue"


@pytest.mark.parametrize(["eq_text", "attribute", "value"], [
    ("cost = 0", "cost", "0"),
    ("cost = 1", "cost", "1"),
    ("cost = 1.5", "cost", "1.5"),
    ("cost = -1.23", "cost", "-1.23"),
    ("f00_bAr  = 3 ", "f00_bAr", "3"),
    ("foo = \"bar\"", "foo", "\"bar\""),
    ("foobar =  ENUM_ENTRY ", "foobar", "ENUM_ENTRY"),
    ("bar =\tarray[7,  (i+1)]", "bar", "array[7,  (i+1)]")
])
def test_can_parse_attribute_equation(parser_for_string, eq_text: str, attribute: str, value: str):
    # given a valid attribute equation string
    # when parsing the string
    ctx = parser_for_string(eq_text).attributeEq()

    # then the context should contain the right data
    assert ctx.ID().getText() == attribute
    attr_ctx = ctx.attributeValue()
    assert ctx.parser.getTokenStream().getText(attr_ctx.start, attr_ctx.stop) == value


@pytest.mark.parametrize(["combinator_char", "combinator_type"], [
    ("->", WorkflowParser.RARROW),
    ("→", WorkflowParser.RARROW),
    ("|", WorkflowParser.PARALLEL),
    ("+", WorkflowParser.CHOICE),
])
def test_can_parse_simple_composite_graphs(parser_for_string, combinator_char: str, combinator_type: int):
    # given a valid workflow graph description with a combination of two tasks
    graph_text = f"A {combinator_char} B"

    # when parsing the string
    ctx = parser_for_string(graph_text).graph()

    # the context should contain the two tasks as subgraphs and the correct type should be detected
    assert len(ctx.graph()) == 2
    assert ctx.op.type == combinator_type


def test_can_parse_singleton_graph(parser_for_string: ParserForStringFixture):
    # given a valid workflow graph description with only one task
    graph_text = "\n Abcd123 "

    # when parsing the string
    ctx = parser_for_string(graph_text).graph()

    # the context should contain the task
    assert len(ctx.graph()) == 0
    assert ctx.ID().getText() == "Abcd123"


def test_can_parse_paranthesed_graph(parser_for_string: ParserForStringFixture):
    # given a valid workflow graph description with parantheses changing precedence
    graph_text = "(A | B) -> C"

    # when parsing the string
    ctx = parser_for_string(graph_text).graph()

    # then the left subgraph should contain the C only
    assert len(ctx.graph()) == 2
    assert ctx.graph(1).getText() == "C"


def test_can_parse_int_looped_graph(parser_for_string: ParserForStringFixture):
    # given a simple looped graph
    graph_text = "A^5"

    # when parsing the string
    ctx = parser_for_string(graph_text).graph()

    # then the number of iterations should be 5 and the subgraph should exist
    assert ctx.graph() is not None
    assert ctx.op.type == WorkflowLexer.EXP
    assert ctx.iterations.text == "5"


def test_can_parse_null_graph(parser_for_string: ParserForStringFixture):
    # given a null graph
    graph_text = "NuLL"

    # when parsing it
    ctx = parser_for_string(graph_text).graph()

    # then the graph should be detectable as null graph
    assert ctx.op.type == WorkflowLexer.NULL


def test_can_parse_optional_graph(parser_for_string: ParserForStringFixture):
    # given a null graph
    graph_text = "(A + B)?"

    # when parsing it
    ctx = parser_for_string(graph_text).graph()

    # then the graph should be detectable as optional
    assert ctx.op.type == WorkflowLexer.QUESTIONMARK
    assert ctx.graph() is not None
    assert len(ctx.graph()) == 1


def test_loop_has_highest_precedence(parser_for_string: ParserForStringFixture):
    # given a sequential composition of singleton and looped graphs
    graph_text = "A B ^ 5"

    # when parsing the string
    ctx = parser_for_string(graph_text).graph()

    # then there should be two subgraphs and the right one is B^5
    assert len(ctx.graph()) == 2
    assert ctx.graph(1).getText() == "B^5"


def test_can_parse_aggregation_with_two_aggregators(parser_for_string: ParserForStringFixture):
    # given a simple aggregation
    agg_text = "aggregation testAgg: sum_pow4, prod_min;"

    # when parsing the string
    ctx = parser_for_string(agg_text).aggregation()

    # then the two IDs should hvae been recognized
    assert ctx.ID(0).getText() == "testAgg"
    assert ctx.name.text == "testAgg"
    assert ctx.ID(1).getText() == "sum_pow4"
    assert ctx.ID(2).getText() == "prod_min"


def test_can_parse_aggregation_with_one_aggregator(parser_for_string: ParserForStringFixture):
    # given a simple aggregation
    agg_text = "aggregation testAgg: sum_pow4;"

    # when parsing the string
    ctx = parser_for_string(agg_text).aggregation()

    # then the two IDs should hvae been recognized
    assert ctx.ID(0).getText() == "testAgg"
    assert ctx.name.text == "testAgg"
    assert ctx.ID(1).getText() == "sum_pow4"


def test_can_parse_attribute_declaration_with_name_only(parser_for_string: ParserForStringFixture):
    # given an attribute declaration with name only
    attr_text = "attribute testAttr;"

    # when parsing the string
    ctx = parser_for_string(attr_text).attributeDecl()

    # then the IDs should be parsed correctly
    assert ctx.ID().getText() == "testAttr"
    assert ctx.name.text == "testAttr"
    assert ctx.attributeType() is None
    assert ctx.default is None


def test_can_parse_attribute_declaration_with_type(parser_for_string: ParserForStringFixture):
    # given an attribute declaration for ints
    attr_text = "attribute testAttr of int;"

    # when parsing the string
    ctx = parser_for_string(attr_text).attributeDecl()

    # then the IDs should be parsed correctly
    assert ctx.name.text == "testAttr"
    assert ctx.attributeType().getText() == "int"
    assert ctx.default is None


def test_can_parse_attribute_declaration_with_default(parser_for_string: ParserForStringFixture):
    # given an attribute declaration with defaultt value
    attr_text = "attribute testAttr default 4.3;"

    # when parsing the string
    ctx = parser_for_string(attr_text).attributeDecl()

    # then the default should have been parsed
    assert ctx.default.getText() == "4.3"


def test_can_parse_attribute_declaration_with_type_end_default(parser_for_string: ParserForStringFixture):
    # given an attribute declaration for ints with default
    attr_text = "attribute testAttr of int default 5;"

    # when parsing the string
    ctx = parser_for_string(attr_text).attributeDecl()

    # then the name, type and default should be parsed correctly
    assert ctx.name.text == "testAttr"
    assert ctx.attributeType().getText() == "int"
    assert ctx.default.getText() == "5"


def test_can_parse_attribute_declaration_with_range_type(parser_for_string: ParserForStringFixture):
    # given an attribute declaratin with type 1..6
    attr_text = "attribute diceResult of 1..6;"

    # when parsing the string
    ctx = parser_for_string(attr_text).attributeDecl()

    # then the type and name should be parsed correctly
    assert ctx.name.text == "diceResult"
    assert ctx.attributeType().getText() == "1..6"
    assert ctx.default is None


def test_can_parse_workflow_declaration_with_name(parser_for_string: ParserForStringFixture):
    # given a simple workflow declaration with name
    wf_text = """workflow wf1: {
        graph: A;
        provision pA for A: cost = 5;
    };"""

    # when parsing the string
    ctx = parser_for_string(wf_text).workflowDecl()

    # then the name should have been parsed
    assert ctx.name.text == "wf1"
    assert ctx.workflow() is not None


def test_can_parse_workflow_declaration_without_name(parser_for_string: ParserForStringFixture):
    # given a simple wf declaration without name
    wf_text = "workflow { graph: A; provision pA for A: cost = 5;};"

    # when parsing the string
    ctx = parser_for_string(wf_text).workflowDecl()

    # then there should be no name but a workflow
    assert ctx.name is None
    assert ctx.workflow() is not None


def test_can_parse_category(parser_for_string: ParserForStringFixture):
    # given a simple categorization
    text = "category provider;"

    # when parsing it
    ctx = parser_for_string(text).category()

    # then there should be the correct name
    assert ctx.name is not None
    assert ctx.name.text == "provider"


def test_can_parse_mixed_tlds(parser_for_string: ParserForStringFixture) -> None:
    # given two tlds
    text = "workflow { graph: A; }; minizinc { int: x = 5; };"

    # when parsing it
    ctx = parser_for_string(text).tlds()

    # then there should be one workflow tld and one minizinc tld
    assert len(ctx.topLevelDeclaration()) == 2
    wf_tld = ctx.topLevelDeclaration(0).workflowDecl()
    assert wf_tld is not None
    mzn_tld = ctx.topLevelDeclaration(1).minizincDecl()
    assert mzn_tld is not None


def test_can_parse_minizinc_tld(parser_for_string: ParserForStringFixture) -> None:
    # given a minizinc block
    text = textwrap.dedent("""\
    minizinc {
    \tvar int: x;
    \tconstraint x < 5;
    }""")

    # when parsing it
    ctx = parser_for_string(text).minizincDecl()

    # then there should be the correct content inside
    start: int = ctx.bodyStart.tokenIndex + 1
    stop: int = ctx.bodyStop.tokenIndex - 1
    actual = ctx.parser.getTokenStream().getText(start, stop)
    assert actual == """\n\tvar int: x;\n\tconstraint x < 5;\n"""
