import pytest
from antlr4.error.ErrorListener import ErrorListener


class FailingErrorListener(ErrorListener):
    def __init__(self, fail_on_error: bool = True,
                 fail_on_ambiguity: bool = False,
                 fail_on_attempting_full_context: bool = False,
                 fail_on_context_sensitivity: bool = False) -> None:
        self.fail_on_error = fail_on_error
        self.fail_on_ambiguity = fail_on_ambiguity
        self.fail_on_attempting_full_context = fail_on_attempting_full_context
        self.fail_on_context_sensitivity = fail_on_context_sensitivity

    def syntaxError(self, recognizer, offending_symbol, line, column, msg, e):
        if self.fail_on_error:
            pytest.fail(f"parsing error at {line}:{column}: {msg}")

    def reportAmbiguity(self, recognizer, dfa, start_index, stop_index, exact, ambig_alts, configs):
        if self.fail_on_ambiguity:
            pytest.fail(f"ambiguity between {start_index} and {stop_index}: {ambig_alts}")

    def reportAttemptingFullContext(self, recognizer, dfa, start_index, stop_index, conflicting_alts, configs):
        if self.fail_on_attempting_full_context:
            pytest.fail(f"attempting full context between {start_index} and {stop_index}: {conflicting_alts}")

    def reportContextSensitivity(self, recognizer, dfa, start_index, stop_index, prediction, configs):
        if self.fail_on_context_sensitivity:
            pytest.fail(f"context sensitivity between {start_index} and {stop_index}: {prediction}")
