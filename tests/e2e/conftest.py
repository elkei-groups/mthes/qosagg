import os

import pytest

from tests.e2e.minizinc_executor import MinizincExecutor


@pytest.fixture(scope="session")
def mzn_exec():
    try:
        return MinizincExecutor(mzn_executable=os.environ["MINIZINCBIN"])
    except KeyError:
        return MinizincExecutor()
