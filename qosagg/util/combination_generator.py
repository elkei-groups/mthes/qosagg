import itertools
from typing import Iterator, Tuple, Iterable


def generate_combinations(counts: Iterable[int]) -> Iterator[Tuple[int]]:
    return itertools.product(*(range(count) for count in counts))  # type: ignore
